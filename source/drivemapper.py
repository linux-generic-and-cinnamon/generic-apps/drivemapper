#!/usr/bin/python3
"""Graphical mount/unmount for networked drives."""
# Disable linter excessive warnings:
# tabs for spaces, unused argument, fnct. name, assgnm. spaces,
# line too long, anomalous backslash, redefining name
# pylint: disable=W0312,W0613,C0103,C0326,C0301,W1401,W0621
##===============================================================
# 🄯 Drugwash, 2023.03.04 - 12.07, v1.0.19 alpha
# License: GPL 2.0+ / WTFPL v2
##===============================================================
##	IMPORTS
##===============================================================
from datetime import datetime
from html import escape
from pwd import getpwnam
# from pathlib import Path
import grp
import configparser
import os
import re
import socket
import subprocess
import time
import sys
import platform
import inspect
import gi
gi.require_version('Gdk', '3.0')
gi.require_version('Gtk', '3.0')
gi.require_version('GdkPixbuf', '2.0')
gi.require_version('Notify', '0.7')
from gi.repository import Notify
from gi.repository import Gdk, GdkPixbuf, GLib, GObject, Gtk
cmd_folder = os.path.realpath(os.path.abspath(os.path.split(inspect.getfile( inspect.currentframe() ))[0]))
if cmd_folder not in sys.path:
	sys.path.insert(0, cmd_folder)
print(cmd_folder)
import var as ai
'''
## import AppIndicator3 for tray icon
try:
	gi.require_version('AppIndicator3', '0.1')
	from gi.repository import AppIndicator3 as appindicator
except Exception as err:
	print(err)
#	pass
'''
## import wakeonlan for WOL
try:
	from wakeonlan import send_magic_packet as wake1
except ModuleNotFoundError as err:
	print(err, "Downloading wakeonlan...")
	os.system('pip install wakeonlan')
	from wakeonlan import send_magic_packet as wake1
## import pywake as fallback for WOL
try:
	import pywake
except ModuleNotFoundError as err:
	print(err, "Downloading pywake...")
	os.system('pip install pywake')
	import pywake
## import continuous_threading for timers
try:
	import continuous_threading
except ModuleNotFoundError as err:
	print(err, "Downloading continuous_threading...")
	os.system('pip install continuous_threading')
	import continuous_threading
## import dbus for notifications
try:
	import dbus
	from dbus.mainloop.glib import DBusGMainLoop
except ModuleNotFoundError:
	print("Downloading dbus-python...")
	os.system("pip install dbus-python")
	import dbus
	from dbus.mainloop.glib import DBusGMainLoop
## import packaging for version comparisons
try:
	import packaging.version as version
#	from packaging import version
except ModuleNotFoundError:
	print("Downloading packaging...")
	os.system("pip install packaging")
#	import packaging
#	from packaging import version
	import packaging.version as version
'''
## print dependencies
for m in ["gi.repository", "pathlib", "os", "sys", "configparser", "subprocess"]:
	try:
		l = sys.modules[m].__file__
		r = subprocess.check_output("dpkg -S " + l, shell=True).decode()
		print(r)
	except:
		pass
'''
# Compare versions
def isMinVer(crtv, minv) -> bool:
	"""Check min Python version and return true/false"""
	return version.parse(crtv) >= version.parse(minv)

debug		= True
##===============================================================
##	DEBUG HELPERS
##===============================================================
if debug:
	os.environ['GDK_DEBUG'] = 'all'
	os.environ['GTK_DEBUG'] = 'all'
	os.environ['G_DEBUG'] = 'fatal-warnings'
	os.environ['G_MESSAGES_DEBUG'] = 'all'
##===============================================================
##	GLOBAL VARIABLES
##===============================================================
APPNAME		= "Drive mapper"
APPVERSION	= "1.0.19 alpha"
RELDATE		= "2023.12.07"
ICONAME		= "drive-mapper"

change		= False
ConfWin		= None
PrefWin		= None
EditWin		= None
AboutWin	= None
pending		= {}	# dictionary for pending mount/unmount operations

RUSER		= os.getlogin()
USER		= str(getpwnam(RUSER).pw_uid)
GROUP		= str(grp.getgrnam(RUSER)[2])

cfp = os.path.realpath(__file__)	# current file path
CD = os.path.dirname(cfp)			# current directory
DEFCFG = os.path.join(CD, "defconfig.ini")
if 'XDG_CONFIG_HOME' in os.environ:
	homecfg = os.environ['XDG_CONFIG_HOME']
else:
	homecfg = os.path.join(os.environ['HOME'], '.config')
cfgpath = os.path.join(homecfg, 'drivemapper')
if not debug:
	CONFIG = os.path.join(cfgpath, "config.ini")
	os.makedirs(cfgpath, exist_ok=True)
else:
	CONFIG = os.path.join(CD, "config.ini")
	print(CONFIG, "\n" + DEFCFG)
appcfg = configparser.RawConfigParser(
	allow_no_value=True, inline_comment_prefixes=('#'))
appcfg.optionxform = str
if not os.path.isfile(CONFIG):
	print("there is no config file")
	if os.path.isfile(DEFCFG):
		print("read from Default config file")
		f = open(DEFCFG)
		appcfg.read_file(f)
		f.close()
		print("write Preferences to new config file")
		with open(CONFIG, "w") as cf:
			appcfg.write(cf)
		cf.close()
		appcfg.clear()
		print("finished creating new config file")
## some file system may be lazy, we DO want to read from the config file
		time.sleep(2)
if os.path.isfile(CONFIG):
	appcfg.read(CONFIG)
	if not appcfg.has_section("Preferences"):
		print("config has no Preferences section")
		if os.path.isfile(DEFCFG):
			appcfg.clear()
			print("read Default config file")
			f = open(DEFCFG)
			appcfg.read_file(f)
			f.close()
			print("read current config file")
			appcfg.read(CONFIG)
			print("write updated data to current config file")
			with open(CONFIG, "w") as cf:
				appcfg.write(cf)
			cf.close()
			print("finished updating config")
		else:
			print("cannot find Default config file")
	else:
		print("config claims it does have Preferences section")

if CD == "/usr/share/drivemapper":
	ipath = os.path.join("/usr/share/pixmaps", ICONAME)
else:
	ipath = os.path.join(CD, ICONAME)
mainIcon = GdkPixbuf.Pixbuf.new_from_file(ipath + ".png")
## Gtk.IconSize.MENU/BUTTON/SMALL_TOOLBAR/LARGE_TOOLBAR/DND/DIALOG
iSSz = Gtk.IconSize.SMALL_TOOLBAR
iNSz = Gtk.IconSize.LARGE_TOOLBAR
iLSz = Gtk.IconSize.DIALOG
# https://stackoverflow.com/questions/61375905/gtk3-button-reduce-padding-of-internal-label
SCREEN			= Gdk.Screen.get_default()
provider		= Gtk.CssProvider()
style_context	= Gtk.StyleContext()
style_context.add_provider_for_screen(
	SCREEN, provider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)
provider.load_from_data("#btnimg {padding: 1px;}".encode())
# <===
# get current time (for debug purposes mainly)
tnow = datetime.now().strftime("%H:%M:%S")
print(APPNAME, "started at", tnow)
#nmapCmnd = "sudo nmap -sn -PE -PA21,22,23,80,445,3389"
nmapCmnd = "sudo nmap -sn -PE -PA80,135,139,445"
#nmapCmnd = "LANGUAGE=ro_RO.UTF-8 sudo nmap -sn -PE -PA21,22,23,80,445,3389" # try LC_ALL=en_US
#nmapCmnd = "sudo nmap -sn -PE -PA21,80,445" # 21,53,79,80,135,139,445,515,554,631,
replex	= "'/^Nmap scan report for .*/{$!N;/Host is up.*latency/{$!N;/MAC Address/{s/.*report for \(.* (.*)\|.*\)\\nHost is.*\\nMAC Address: \(.*\)\s(\(.*\))/\\1\t\\2\t\\3/}}};/Starting [Nn]map/d;/[Nn]map done/d;s/^Nmap scan report for \(.*\)\s(\(.*\)).*/\\1 \\2\tMAC not found\tUnknown/;/^$/d'"
replsh	= "'s/\t\(.*\)\s\{4,\}Disk.*/\\1/;/Sharename/d;/Reconnecting/d;/failed (Error/d;/Failed to connect/d;/Server/d;/Workgroup/d;/----/d;/ Printer /d;s/.*\$\s.*//;/^$/d;/\s\{9,\}/d'"
replmt	= "'s/^\/\/\(.*\) on \(.*\) type \(.*\) (\(.*\))/\\1\t\\2\t\\3\t\\4/'"
## Translatable strings
msg_btn_active	 = "Connected"
msg_btn_inactive = "Disconnected"
scanErrMsg	= "Scanning has returned corrupt/incomplete results. \
\nPlease repeat the operation or restart application."
umntMsg		= "Simple unmount would fail because the mounted location \
\nis currently in use by at least one process.\n\nWhat type \
of unmount would you choose further?"
warnMsg		= "Warning"
badpathMsg	= "The chosen path %s cannot be\nused for mounting network drives.\n \
Please choose a proper path\n( i.e. /media/%s )\nCreate folder/path if it doesn't exist."
fbdmntMsg	= "Mount path forbidded!"
rementMsg	= "Are you sure you want to remove the entry <b>%s</b> for\n<b>//%s/%s</b> mounted at <b>%s</b> ?"
rement2Msg	= "Are you sure you want to remove the entry for\n<b>//%s/%s</b> ?"
PICOYES=PICONO=PICOMBY=PICOOFF=PICOPRO=None
ICOYES=ICONO=ICOMBY=ICOOFF=ICOPRO=ICORSC=None
ICOOK=ICOCCL=ICOINF=ICOQST=ICOWRN=ICOERR=None
ICONEW=ICOADD=ICOEDT=ICORMV=ICOHLP=ICOHID=ICOEXT=None
AsortBy=click1=MLiSz=noResize=prefIp=prefLstIp=None
sortedList=sortBy=timerChk=timerInt=useAppInd=mainWidth=None
mainHeight=mainX=mainY=keepSize=keepPos=retries=None
notif = None
##===============================================================
##	GLOBAL FUNCTIONS
##===============================================================
def readDefaults():
	"""Read default values for Preferences."""
	## alternatives for Preferences are loaded from vars as ai
	## regular icons
	ICOYES		= "media-record"
	ICONO		= "media-playback-stop"
	ICOMBY		= "media-playback-pause"
	ICOOFF		= "media-eject"
	ICOPRO		= "media-playback-start"
	ICORSC		= "gtk-refresh"
	ICOOK		= "gtk-ok"
	ICOCCL		= "gtk-cancel"
	ICOINF		= "gtk-dialog-info"
	ICOQST		= "gtk-dialog-question"
	ICOWRN		= "gtk-dialog-warning"
	ICOERR		= "gtk-dialog-error"
	ICONEW		= "gtk-new"
	ICOADD		= "gtk-add"
	ICOEDT		= "gtk-edit"
	ICORMV		= "gtk-delete"
	ICOHLP		= "gtk-help"
	ICOHID		= "window-minimize-symbolic"
	ICOEXT		= "gtk-close"
#	AsortBy		= "host, entry, status, ip"
	click1		= True		# use single click to select/activate
	MLiSz		= 32		# main list icon size - we should read this value from the config sometime
	noResize	= False		# fixed size for main window
	prefIp		= False		# prefer mounting by IP instead of host+share
	prefLstIp	= False		# prefer IP as default host when adding/editing entries
	sortedList	= True		# sort icon list
	sortBy		= "host"	# sorting column in main list (host/entry/status/ip)
	timerChk	= True		# periodically check for mounts status
	timerInt	= 1			# timer interval [minutes]
	useAppInd	= False		# use AppIndicator instead of GtkStatusIcon
	mainWidth	= 0			# last known width of main window
	mainHeight	= 0			# last known height of main window
	mainX		= -1
	mainY		= -1
	keepSize	= True		# store main window size between sessions
	keepPos		= True
	retries		= 5			# amount before giving up delayed mount/unmount
	notifTout	= 3.5		# [sec] amount to show notification on screen
	globals().update(locals())

def readPreferences():
	"""Read values from Preferences."""
	if not appcfg:
		print("no appcfg in readPreferences()")
	## alternatives for Preferences are in vars.py
	## regular icons
	ICOYES		= appcfg.get("Preferences", "ICOYES", fallback="media-record")
	ICONO		= appcfg.get("Preferences", "ICONO"	, fallback="media-playback-stop")
	ICOMBY		= appcfg.get("Preferences", "ICOMBY", fallback="media-playback-pause")
	ICOOFF		= appcfg.get("Preferences", "ICOOFF", fallback="media-eject")
	ICOPRO		= appcfg.get("Preferences", "ICOPRO", fallback="media-playback-start")
	ICORSC		= appcfg.get("Preferences", "ICORSC", fallback="gtk-refresh")
	ICOOK		= appcfg.get("Preferences", "ICOOK"	, fallback="gtk-ok")
	ICOCCL		= appcfg.get("Preferences", "ICOCCL", fallback="gtk-cancel")
	ICOINF		= appcfg.get("Preferences", "ICOINF", fallback="gtk-dialog-info")
	ICOQST		= appcfg.get("Preferences", "ICOQST", fallback="gtk-dialog-question")
	ICOWRN		= appcfg.get("Preferences", "ICOWRN", fallback="gtk-dialog-warning")
	ICOERR		= appcfg.get("Preferences", "ICOERR", fallback="gtk-dialog-error")
	ICONEW		= appcfg.get("Preferences", "ICONEW", fallback="gtk-new")
	ICOADD		= appcfg.get("Preferences", "ICOADD", fallback="gtk-add")
	ICOEDT		= appcfg.get("Preferences", "ICOEDT", fallback="gtk-edit")
	ICORMV		= appcfg.get("Preferences", "ICORMV", fallback="gtk-delete")
	ICOHLP		= appcfg.get("Preferences", "ICOHLP", fallback="gtk-help")
	ICOHID		= appcfg.get("Preferences", "ICOHID", fallback="window-minimize-symbolic")
	ICOEXT		= appcfg.get("Preferences", "ICOEXT", fallback="gtk-close")
#	AsortBy		= appcfg.get("Preferences", "AsortBy", fallback="host, entry, status, ip")
	click1		= appcfg.getboolean	("Preferences"	, "click1"		, fallback=True)
	MLiSz		= appcfg.getint		("Preferences"	, "MLiSz"		, fallback=32)
	noResize	= appcfg.getboolean	("Preferences"	, "noResize"	, fallback=False)
	prefIp		= appcfg.getboolean	("Preferences"	, "prefIp"		, fallback=False)
	prefLstIp	= appcfg.getboolean	("Preferences"	, "prefLstIp"	, fallback=False)
	sortedList	= appcfg.getboolean	("Preferences"	, "sortedList"	, fallback=True)
	sortBy		= appcfg.get		("Preferences"	, "sortBy"		, fallback="host")
	timerChk	= appcfg.getboolean	("Preferences"	, "timerChk"	, fallback=False)
	timerInt	= appcfg.getint		("Preferences"	, "timerInt"	, fallback=1)
	useAppInd	= appcfg.getboolean	("Preferences"	, "useAppInd"	, fallback=False)
	mainWidth	= appcfg.getint		("Preferences"	, "mainWidth"	, fallback=0)
	mainHeight	= appcfg.getint		("Preferences"	, "mainHeight"	, fallback=0)
	mainX		= appcfg.getint		("Preferences"	, "mainX"		, fallback=-1)
	mainY		= appcfg.getint		("Preferences"	, "mainY"		, fallback=-1)
	keepSize	= appcfg.getboolean	("Preferences"	, "keepSize"	, fallback=True)
	keepPos		= appcfg.getboolean	("Preferences"	, "keepPos"		, fallback=True)
	retries		= appcfg.getint		("Preferences"	, "retries"		, fallback=5)
	notifTout	= appcfg.getfloat	("Preferences"	, "notifTout"	, fallback=3.5)
	globals().update(locals())

def writePreferences():
	"""Write current values to Preferences."""
	if not appcfg:
		print("no appcfg in writePreferences()")
	if not appcfg.has_section("Preferences"):
		writeCfg()
		appcfg.clear()
		appcfg.add_section("Preferences")
		print("read current config file")
		appcfg.read(CONFIG)
	try:
		appcfg.set("Preferences", "ICOYES"	, ICOYES)
		appcfg.set("Preferences", "ICONO"	, ICONO)
		appcfg.set("Preferences", "ICOMBY"	, ICOMBY)
		appcfg.set("Preferences", "ICOOFF"	, ICOOFF)
		appcfg.set("Preferences", "ICOPRO"	, ICOPRO)
		appcfg.set("Preferences", "ICORSC"	, ICORSC)
		appcfg.set("Preferences", "ICOOK"	, ICOOK)
		appcfg.set("Preferences", "ICOCCL"	, ICOCCL)
		appcfg.set("Preferences", "ICOINF"	, ICOINF)
		appcfg.set("Preferences", "ICOQST"	, ICOQST)
		appcfg.set("Preferences", "ICOWRN"	, ICOWRN)
		appcfg.set("Preferences", "ICOERR"	, ICOERR)
		appcfg.set("Preferences", "ICONEW"	, ICONEW)
		appcfg.set("Preferences", "ICOADD"	, ICOADD)
		appcfg.set("Preferences", "ICOEDT"	, ICOEDT)
		appcfg.set("Preferences", "ICORMV"	, ICORMV)
		appcfg.set("Preferences", "ICOHLP"	, ICOHLP)
		appcfg.set("Preferences", "ICOHID"	, ICOHID)
		appcfg.set("Preferences", "ICOEXT"	, ICOEXT)
#		appcfg.set("Preferences", "AsortBy"		, AsortBy)
		appcfg.set("Preferences", "click1"		, click1)
		appcfg.set("Preferences", "MLiSz"		, MLiSz)
		appcfg.set("Preferences", "noResize"	, noResize)
		appcfg.set("Preferences", "prefIp"		, prefIp)
		appcfg.set("Preferences", "prefLstIp"	, prefLstIp)
		appcfg.set("Preferences", "sortedList"	, sortedList)
		appcfg.set("Preferences", "sortBy"		, sortBy)
		appcfg.set("Preferences", "timerChk"	, timerChk)
		appcfg.set("Preferences", "timerInt"	, timerInt)
		appcfg.set("Preferences", "useAppInd"	, useAppInd)
		appcfg.set("Preferences", "mainWidth"	, mainWidth)
		appcfg.set("Preferences", "mainHeight"	, mainHeight)
		appcfg.set("Preferences", "mainX"		, mainX)
		appcfg.set("Preferences", "mainY"		, mainY)
		appcfg.set("Preferences", "keepSize"	, keepSize)
		appcfg.set("Preferences", "keepPos"		, keepPos)
		appcfg.set("Preferences", "retries"		, retries)
		appcfg.set("Preferences", "notifTout"	, notifTout)
		print("write updated data to current config file")
	except Exception as e:
		print("Error in writePreferences(): %s" % e)
	writeCfg()

class MountItem():
	"""Initialize new item values to empty string."""
	v0 = v1 = v2 = v3 = v4 = v5 = v6 = v7 = ""
	v8 = v9 = va = vb = vc = vd = ve = ""

def readCfg(obj=None):
	"""Read configuration from file."""
	if not appcfg:
		print("no appcfg in readCfg()")
	appcfg.clear()
	if os.path.isfile(CONFIG):
		appcfg.read(CONFIG)
		if debug:
			print("read config from", CONFIG)

def readEntry(i):
	"""Read item parameters."""
	try:
		e = str(i)
		mntitem.v0 = appcfg.get(e, "label")
		mntitem.v1 = appcfg.get(e, "server")
		mntitem.v2 = appcfg.get(e, "share")
		mntitem.v3 = appcfg.get(e, "local")
		mntitem.v4 = appcfg.get(e, "user"	, fallback="")
		mntitem.v5 = appcfg.get(e, "psw"	, fallback="")
		mntitem.v6 = appcfg.get(e, "type")
		mntitem.v7 = appcfg.get(e, "version", fallback="")
		mntitem.v8 = appcfg.get(e, "fmode"	, fallback="")
		mntitem.v9 = appcfg.get(e, "dmode"	, fallback="")
		mntitem.va = appcfg.get(e, "umask"	, fallback="")
		mntitem.vb = appcfg.get(e, "sec"	, fallback="")
		mntitem.vc = appcfg.get(e, "ip"		, fallback="")
		mntitem.vd = appcfg.get(e, "mac"	, fallback="")
		mntitem.ve = appcfg.get(e, "brand"	, fallback="")
#		if debug:
#			print("read entry", e)
		return True
	except Exception:
		mntitem.v0 = mntitem.v1 = mntitem.v2 = mntitem.v3 = ""
		mntitem.v4 = mntitem.v5 = mntitem.v6 = mntitem.v7 = ""
		mntitem.v8 = mntitem.v9 = mntitem.va = mntitem.vb = ""
		mntitem.vc = mntitem.vd = mntitem.ve = ""
		if not appcfg:
			print("no appcfg in readEntry(%d)" % i)
		if debug:
			print("section", e, "doesn't exist")
		return False

def addSection(i, edit=False):
	"""Add item/section to configuration."""
	if not appcfg:
		print("no appcfg in addSection(%d, %s)" % (i, edit))
	e = str(i)
	if not edit:
		appcfg.add_section(e)
	appcfg.set(e, "label"	, mntitem.v0)
	appcfg.set(e, "server"	, mntitem.v1)
	appcfg.set(e, "share"	, mntitem.v2)
	appcfg.set(e, "local"	, mntitem.v3)
	appcfg.set(e, "user"	, mntitem.v4)
	appcfg.set(e, "psw"		, mntitem.v5)
	appcfg.set(e, "type"	, mntitem.v6)
	appcfg.set(e, "version"	, mntitem.v7)
	appcfg.set(e, "fmode"	, mntitem.v8)
	appcfg.set(e, "dmode"	, mntitem.v9)
	appcfg.set(e, "umask"	, mntitem.va)
	appcfg.set(e, "sec"		, mntitem.vb)
	appcfg.set(e, "ip"		, mntitem.vc)
	appcfg.set(e, "mac"		, mntitem.vd)
	appcfg.set(e, "brand"	, mntitem.ve)

def writeCfg():
	"""Write current configuration to file."""
	if not appcfg:
		print("no appcfg in writeCfg() for %s" % CONFIG)
	with open(CONFIG, "w") as cfg:
		appcfg.write(cfg)
	cfg.close()

def updateIcons(obj=None):
	"""Update icons when theme changed."""
	iTheme	= Gtk.IconTheme.get_for_screen(SCREEN)
	PICOYES	= iTheme.load_icon_for_scale(ICOYES, 32, 1, 0)
	PICONO	= iTheme.load_icon_for_scale(ICONO, 32, 1, 0)
	PICOMBY	= iTheme.load_icon_for_scale(ICOMBY, 32, 1, 0)
	PICOOFF	= iTheme.load_icon_for_scale(ICOOFF, 32, 1, 0)
	PICOPRO	= iTheme.load_icon_for_scale(ICOPRO, 32, 1, 0)
	globals().update(locals())

## https://pychao.com/2021/03/01/sending-desktop-notification-in-linux-with-python-with-d-bus-directly/
## https://github.com/digitaltrails/procno/blob/master/procno.py
class Toast():
	"""Toast-type notification."""
	def __init__(self):
		addr = "org.freedesktop.Notifications"
		self.nif = dbus.Interface(
			dbus.SessionBus(mainloop=DBusGMainLoop(set_as_default=True)).get_object(
			# dbus.SessionBus().get_object(
			addr, "/"+addr.replace(".", "/")), addr)
		print("Using Toast():\n", self.nif)
		self.caps = list([str(s) for s in self.nif.GetCapabilities()])
		if debug:
			print("Toast-Notification caps:", self.caps)
		self.hints = {"urgency": 1}
#		'''
		if "body_markup" in self.caps:
			self.hints["body_markup"]=True
			self.body = APPNAME + " <b>" + APPVERSION + "</b> has started"
		else:
			self.body = APPNAME + " " + APPVERSION + " has started"
		if "persistence" in self.caps:
			self.hints["persistence"]=False
		self.hints["transient"]=True
		if "sound" in self.caps:
			self.hints["sound-name"]="dialog-information" # device-added, device-removed
#		'''
		self.title = "Information"
	def update(self, ttl: str, msg: str, snd: str="dialog-information"):
		"""Update toast message strings."""
		self.title = ttl
		self.body = msg
		if "sound" in self.caps:
			self.hints["sound-name"] = snd
	def show(self):
		if debug:
			print("notifTout=", int(notifTout*1000), "ms")
		"""Show toast notification."""
		self.nif.Notify(APPNAME, 0, ICONAME
			, escape(self.title).encode('UTF-8')
			, escape(self.body).encode('UTF-8')
			, [], self.hints, int(notifTout*1000))
	def uninit(self):
		"""Unload toast."""
		# GObject.Object.unref(self.nif)
		...

def initNotifier():
	"""Initialize either Notifier module."""
	global notif, notifTout
	try:
		notif = Toast()
		tt = notifTout
		notifTout = 4.5
		notif.show()
		notifTout = tt
	except Exception as err:
		print("Toast class error:\n", err, "\n")
		subprocess.run("sudo /usr/lib/notification-daemon/notification-daemon &", check=True, shell=True)
		# subprocess.check_output(["sudo", "/usr/lib/notification-daemon/notification-daemon", "&"], shell=True)
		Notify.init(APPNAME)
		caps = Notify.get_server_caps()
		if debug:
			print("Notify-Notification caps:", caps)
		notif = Notify.Notification.new("Information", APPNAME + \
			" <b>" + APPVERSION + "</b> has started")
		notif.set_image_from_pixbuf(mainIcon)
		notif.set_urgency(Notify.Urgency.NORMAL) # LOW/NORMAL/CRITICAL
		notif.set_timeout(Notify.EXPIRES_DEFAULT) # Notify.EXPIRES_DEFAULT/NEVER
		if "actions" in caps:
			notif.add_action("close", "Close", notifCB, None) # a data object can be passed last
		if debug:
			print("Notify timeout set to notifTout=", int(notifTout*1000), "ms")
		if "body_markup" in caps:
			notif.body_markup=True
		if "persistence" in caps:
			notif.persistence=False
		notif.set_hint('transient', GLib.Variant.new_boolean(True))
		if "sound" in caps:
			notif.set_hint('sound-name', GLib.Variant.new_string("device-added")) # device-removed
		notif.show()
		notif.set_timeout(notifTout*1000) # Notify.EXPIRES_DEFAULT/NEVER

def notifCB(nObj, action, data):
	if debug:
		print(nObj, action, data)

## QUESTION
def MsgBoxQst(wnd, title, msg, txt):
	"""MessageBox Question."""
	mdlg = Gtk.MessageDialog(
		transient_for=wnd,
		modal=True,
		## INFO/WARNING/QUESTION/ERROR/OTHER
		message_type=Gtk.MessageType.QUESTION,
		buttons=Gtk.ButtonsType.NONE,
		title=APPNAME + " - " + title,
		text=txt
	)
	mdlg.format_secondary_markup(msg)
	mdlg.add_buttons("Yes", Gtk.ResponseType.YES
		, "No", Gtk.ResponseType.NO)
	img = Gtk.Image.new_from_icon_name(ICOHLP,iLSz)
	img.show()
	mdlg.set_image(img) # Deprecated but still works!
	res = mdlg.run()
	mdlg.destroy()
	return res

## WARNING
def MsgBoxWrn(wnd, title, msg, txt):
	"""MessageBox Warning."""
	mdlg = Gtk.MessageDialog(
		transient_for=wnd,
		modal=True,
		message_type=Gtk.MessageType.WARNING,
		buttons=Gtk.ButtonsType.OK,
		title=APPNAME + " - " + title,
		text=txt
	)
	mdlg.format_secondary_markup(msg)
	img = Gtk.Image.new_from_icon_name(ICOWRN,iLSz)
	img.show()
	mdlg.set_image(img) # Deprecated but still works!
	mdlg.run()
	mdlg.destroy()

## ERROR
def MsgBoxErr(wnd, title, msg):
	"""MessageBox Error."""
	mdlg = Gtk.MessageDialog(
		transient_for=wnd,
		modal=True,
		message_type=Gtk.MessageType.ERROR,
		buttons=Gtk.ButtonsType.OK,
		title=APPNAME + " - " + title,
		text="Operation error"
	)
	mdlg.format_secondary_markup(msg)
	img = Gtk.Image.new_from_icon_name(ICOERR,iLSz)
	img.show()
	mdlg.set_image(img) # Deprecated but still works!
	mdlg.run()
	mdlg.destroy()
##===============================================================
## MAIN WINDOW GUI
##===============================================================
class MainWindow(Gtk.ApplicationWindow):
	"""Main application window shows configured entries."""
	def __init__(self, appl):
		"""Main initialization."""
		super(MainWindow, self).__init__(title=APPNAME, application=appl)
#		global ConfWin, PrefWin, EditWin, AboutWin
		print("change=%s in MainWindow()" % change)
		self.set_title(APPNAME)
		self.set_icon(mainIcon)
		self.connect("delete-event", self.quitApp)
		self.connect("destroy", self.quitApp)
		self.connect("window-state-event", self.winState)
		## MENU BAR
		self.menubar = Gtk.MenuBar()
		menu1 = Gtk.Menu()
		fmi = Gtk.MenuItem.new_with_label("File")
		cmi = Gtk.MenuItem.new_with_label("Configure")
		cmi.connect("activate", self.configure)
		sep = Gtk.SeparatorMenuItem()
		emi = Gtk.MenuItem.new_with_label("Exit")
		emi.connect("activate", self.quitApp)
		menu1.append(cmi)
		menu1.append(sep)
		menu1.append(emi)
		fmi.set_submenu(menu1)
		menu2 = Gtk.Menu()
		dmi = Gtk.CheckMenuItem.new_with_label("Debug mode")
		dmi.set_active(debug)
		dmi.connect("toggled", self.debugmode)
		tmi = Gtk.CheckMenuItem.new_with_label("Periodic scan")
		tmi.set_active(timerChk)
		tmi.connect("toggled", self.timermode)
		omi = Gtk.MenuItem.new_with_label("Options")
		pmi = Gtk.MenuItem.new_with_label("Preferences")
		pmi.connect("activate", self.preferences)
		sep = Gtk.SeparatorMenuItem()
		menu2.append(dmi)
		menu2.append(tmi)
		menu2.append(sep)
		menu2.append(pmi)
		omi.set_submenu(menu2)
		menu3 = Gtk.Menu()
		hmi = Gtk.MenuItem.new_with_label("Help")
		ami = Gtk.MenuItem.new_with_label("About...")
		ami.connect("activate", self.about)
		menu3.append(ami)
		hmi.set_submenu(menu3)
		self.menubar.add(fmi)
		self.menubar.add(omi)
		self.menubar.add(hmi)
		## TRAY MENU
		self.menutray = Gtk.Menu()
		self.cmi = Gtk.MenuItem.new_with_label("Configure")
		self.cmi.connect("activate", self.configure)
		pmi = Gtk.MenuItem.new_with_label("Preferences")
		pmi.connect("activate", self.preferences)
		sep1 = Gtk.SeparatorMenuItem()
		ami = Gtk.MenuItem.new_with_label("About...")
		ami.connect("activate", self.about)
		sep2 = Gtk.SeparatorMenuItem()
		emi = Gtk.MenuItem.new_with_label("Exit")
		emi.connect("activate", self.quitApp)
		self.menutray.append(self.cmi)
		self.menutray.append(pmi)
		self.menutray.append(sep1)
		self.menutray.append(ami)
		self.menutray.append(sep2)
		self.menutray.append(emi)
		self.cmi.show()
		pmi.show()
		sep1.show()
		ami.show()
		sep2.show()
		emi.show()
		self.systray = None
		self.setSystray()
		## GUI
		self.main = Gtk.Grid(halign=Gtk.Align.FILL, valign=Gtk.Align.FILL
			, expand=True, column_homogeneous=True, margin = 5
			, column_spacing = 3, row_spacing = 2)
		self.main.attach(self.menubar, 0, 0, 4, 1) # left,top,width,height
		## toolbar buttons
		btnRfsh = Gtk.Button(label="Refresh", halign=Gtk.Align.START, margin=1)
		btnRfsh.set_always_show_image(True)
		btnRfsh.set_image(Gtk.Image.new_from_icon_name(ICORSC, iSSz))
		btnRfsh.connect('clicked', self.timerUpdate)

		btnMAll = Gtk.Button(label="Mount all", halign=Gtk.Align.START, margin=1)
		btnMAll.set_always_show_image(True)
		btnMAll.set_image(Gtk.Image.new_from_icon_name(ICOYES, iSSz))
		btnMAll.connect('clicked', self.mountAll)
#		btnMAll.set_sensitive(False)
		btnUAll = Gtk.Button(label="Unmount all", halign=Gtk.Align.START, margin=1)
		btnUAll.set_always_show_image(True)
		btnUAll.set_image(Gtk.Image.new_from_icon_name(ICONO, iSSz))
		btnUAll.connect('clicked', self.unmountAll)
#		btnUAll.set_sensitive(False)

		btnFMAll = Gtk.Button(label="FMount all", halign=Gtk.Align.START, margin=1)
		btnFMAll.set_always_show_image(True)
		btnFMAll.set_image(Gtk.Image.new_from_icon_name(ICOYES, iSSz))
		btnFMAll.connect('clicked', self.mountAll, True)
#		btnFMAll.set_sensitive(False)
		btnFUAll = Gtk.Button(label="FUnmount all", halign=Gtk.Align.START, margin=1)
		btnFUAll.set_always_show_image(True)
		btnFUAll.set_image(Gtk.Image.new_from_icon_name(ICONO, iSSz))
		btnFUAll.connect('clicked', self.unmountAll, True)
#		btnFUAll.set_sensitive(False)

		btnHide = Gtk.Button(label="Hide", halign=Gtk.Align.START, margin=1)
		btnHide.set_always_show_image(True)
		btnHide.set_image(Gtk.Image.new_from_icon_name(ICOHID, iSSz))
		btnHide.connect('clicked', self.hideApp)
		btnExit = Gtk.Button(label="Exit", halign=Gtk.Align.START, margin=1)
		btnExit.set_always_show_image(True)
		btnExit.set_image(Gtk.Image.new_from_icon_name(ICOEXT, iSSz))
		btnExit.connect('clicked', self.quitApp)
		## toolbar
		tbar = Gtk.Toolbar(margin=1)
		tbar.set_show_arrow(False)
		tbar.set_icon_size(iSSz)
		tbar.set_style(Gtk.ToolbarStyle.BOTH_HORIZ)
		b1 = Gtk.ToolItem()
		b1.add(btnRfsh)
		b2 = Gtk.ToolItem()
		b2.add(btnMAll)
		b3 = Gtk.ToolItem()
		b3.add(btnUAll)
		b4 = Gtk.ToolItem()
		b4.add(btnFMAll)
		b5 = Gtk.ToolItem()
		b5.add(btnFUAll)
		b6 = Gtk.ToolItem()
		b6.add(btnHide)
		b7 = Gtk.ToolItem()
		b7.add(btnExit)
		tbar.insert(b1, -1)
		tbar.insert(Gtk.SeparatorToolItem(), -1)
		tbar.insert(b2, -1)
		tbar.insert(b3, -1)
		tbar.insert(Gtk.SeparatorToolItem(), -1)
		tbar.insert(b4, -1)
		tbar.insert(b5, -1)
		tbar.insert(Gtk.SeparatorToolItem(), -1)
		tbar.insert(b6, -1)
		tbar.insert(b7, -1)
		self.main.attach(tbar, 0, 2, 4, 1)
		self.add(self.main)
		self.scroller = Gtk.ScrolledWindow(halign=Gtk.Align.FILL, valign=Gtk.Align.FILL, expand=True)
#		self.scroller.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.AUTOMATIC)
		self.addList()
		self.connect('style-updated', self.refIcons)
#		self.connect('style-set', self.refIcons)
		if timerChk:
			self.th = continuous_threading.PeriodicThread(timerInt*60, self.timerUpdate)
			self.th.start()
		else:
			self.updateList()
		self.show_all()
		self.connect('configure-event', self.mainWndEvent)
		self.popup = None

	def timerUpdate(self, var=None):
		"""Update item list on a timer."""
		GLib.idle_add(lambda: self.updateList())

	def mountAll(self, btn, var=False):
		"""Mount all items in the list."""
		GLib.idle_add(lambda: self.mountGrp(var))

	def unmountAll(self, btn, var=False):
		"""Unmount all items in the list."""
		GLib.idle_add(lambda: self.unmountGrp(var))

	def refIcons(self, widget=None, prev=None):
		"""Refresh all application icons."""
		updateIcons()
		self.refListIcons(self.list)
		self.list.show()

	def refListIcons(self, lst):
		"""Update status icons in the list."""
		mdl = lst.get_model().get_model() if sortedList else lst.get_model() # GtkTreeModel/Sort
		itr = mdl.get_iter_first()
		while itr:
			[state, mach] = mdl.get(itr, 4, 5)
			pxb = PICOYES if state == "on" else PICOOFF if mach == "0" else \
				PICOPRO if mach == "1" else PICONO
			mdl.set_value(itr, 0, pxb)
			itr = mdl.iter_next(itr)

	def addList(self, w=None):
		"""Add item to list."""
		if hasattr(self, 'frame'):
			self.frame.destroy()
		self.list = Gtk.IconView()
		self.list.set_activate_on_single_click(click1)
		self.list.set_selection_mode(Gtk.SelectionMode.SINGLE)
		self.list.props.item_orientation=Gtk.Orientation.HORIZONTAL
		self.list.connect('size-allocate', self.sizeAllocate)
		self.list.connect('item-activated', self.itemClicked)
		self.list.connect('button-press-event', self.itemContext)
		self.list.connect('popup-menu', self.itemContext)
		self.liststore = Gtk.ListStore(GdkPixbuf.Pixbuf, str, str, str, str, str, str)
		if sortedList:
			sc = 2 if sortBy == "host" else 3 if sortBy == "entry" else \
				4 if sortBy == "status" else 5 if sortBy == "ip" else 1
			self.tms = Gtk.TreeModelSort(model=self.liststore)
			self.tms.set_sort_column_id(sc, Gtk.SortType.ASCENDING)
			self.list.set_model(self.tms)
		else:
			self.list.set_model(self.liststore)
		self.list.set_pixbuf_column(0)
		self.list.set_markup_column(1)
		self.list.set_text_column(2)
		self.list.set_text_column(3)
		self.list.set_text_column(4)
		self.list.set_tooltip_column(6)
		self.list.set_columns(-1)
		self.list.set_row_spacing(2)
		self.list.set_column_spacing(3)
		self.list.set_spacing(5)
#		self.list.set_item_width(100)
		self.list.set_item_width(-1)
		self.list.set_item_padding(2)
		self.list.set_margin(2)
#		self.updateList()
		l = Gtk.Label(label="<b>Network mapped drives</b>", use_markup=True)
		self.frame = Gtk.Frame(expand=True)
		self.frame.set_label_widget(l)
		self.frame.set_label_align(0.5, 0.5)
		self.scroller.add(self.list)
		self.frame.add(self.scroller)
		self.main.attach(self.frame, 0, 1, 4, 1)
		self.refIcons()

	def updateList(self, v=None):
		"""Update status in main item list."""
		print("updating...")
		if hasattr(self, 'th'):
			if not timerChk:
				return False
		while Gtk.events_pending():
			Gtk.main_iteration()
		try:
			tim1 = time.perf_counter()
			if os.path.isfile(CONFIG):
				## get current machine's IP(s)
				res = subprocess.check_output("hostname -I", shell=True).decode().rstrip()
				n0 = res.split(' ')
				self.ahosts = {}
				for y in n0:
					i = y.split(".")
					ip = ".".join([i[0], i[1], i[2], "*"])
					## scan the network(s) for all connected machines
					# cmnd = nmapCmnd + " " + ip + " | sed " + replex + " | sed 's/[()]//g' | sort"
					cmnd = " ".join([nmapCmnd, ip, "| sed", replex, "| sed 's/[()]//g' | sort"])
					obj = subprocess.check_output(cmnd, shell=True).decode()
					n1 = obj.split('\n')
#					if debug:
#						print(n1)
					for j in n1:
						if j == "" or ":" not in j: # check for bad host (missing MAC)
							continue
						m = j.split('\t')
						if len(m) < 3:
							MsgBoxErr(self, "Network scan", scanErrMsg)
							return False
						n = lambda: None
						g = m[0].split(' ')
						try:
							n.name = g[0]
							n.ip = g[1]
						except Exception:
							n.name = "Unknown"
							n.ip = g[0]
						n.mac = m[1]
						n.brd = m[2]
#						print(n.name, n.ip, n.mac, n.brd)
						self.ahosts[n.name] = n
				## check for live machines and mounted shares
				rows = []
				sect = appcfg.sections()
				activehosts = {}
#				if debug:
#					x = len(sect)-1 if appcfg.has_section("Preferences") else len(sect)
#					print("we have", x, "sections")
				for i, s in enumerate(sect):
					if s == "Preferences" or not readEntry(s):
						continue
					## remember shares in already scanned hosts in order to
					## reduce scanning time
					if not mntitem.v1 in activehosts:
						cmnd = "smbclient -U \"guest%\" -L //" + mntitem.v1 + " | sed " + replsh + " | sort"
						activehosts[mntitem.v1] = subprocess.check_output(cmnd, shell=True).decode()
					## here we may have to perform additional check for possibly hidden hosts
					# png = None
					for n, h in self.ahosts.items():
#						if debug:
#							print("h", h)
#							print("cur MAC", mntitem.vd, "host MAC", h.mac)
						if h.mac == mntitem.vd:
#							host = h.name
#							print(mntitem.vd, "found")
							png = True
							appcfg.set(s, "ip" , h.ip)
							appcfg.set(s, "mac" , h.mac)
							appcfg.set(s, "brand" , h.brd) # not sure about this
							mntitem.vc = h.ip
							# mntitem.vd = h.mac  # redundant!
							mntitem.ve = h.brd
							break
						png = False
					## we must check if share path exists and is a directory.
					# st = "on" if os.path.ismount(mntitem.v3) else "off" # share state
					## we use host name, as the IP is more likely to change with DHCP
#					rempath = os.path.join("//", mntitem.v1, mntitem.v2)
#					st = self.chkNtwStat(mntitem.v1, mntitem.v2, mntitem.v3)  # share state
					st = self.chkNtwStat(activehosts[mntitem.v1], mntitem.v2, mntitem.v3)  # share state
#					st = self.chkNtwStat(png, mntitem.v3)  # share state
					# mst = "0" if png == False else "1" if png is None else "2" # machine state
					## Machine state is tricky,there can be "hidden" hosts but it's difficult
					## to detect them, and more so to connect to their shares.
					mst = "0" if png is False else "1" if png is None else "2" # machine state
					# pxb = PICOYES if st == "on" else PICOOFF if png == False else \
					# PICOPRO if png is None else PICONO
					## Icon selection is difficult. We currently use:
					## PICOYES	Record = mounted share
					## PICOPRO	Playback = available share
					## PICONO	Stop = Unavailable share
					## PICO???	??? = machine connected, firewalled/hidden
					## PICOMBY	Pause = machine connected, off
					## PICOOFF	Eject = machine disconnected
					pxb = PICOYES if st == "on" else PICOPRO if st == "off" else \
						PICONO if st == "not" and png == True else \
						PICOOFF if png is False else PICOMBY
#					print(rempath, ",st=", st, ",png=", png, ",pxb=", pxb)
					tt = self.setTT(st, mst)
#					self.liststore.append([pxb, "<b>" + mntitem.v0 + "</b>\n<sub>["
#						+ mntitem.v3 + "]</sub>", mntitem.v1, s, st, mst, tt])
					rows.append([pxb, "<b>" + mntitem.v0 + "</b>\n<sub>["
						+ mntitem.v3 + "]</sub>", mntitem.v1, s, st, mst, tt])
					'''
					if debug:
						print("Entry", "%02d" %(i+1), "=>", mntitem.v0, mntitem.v1
							, mntitem.v2, mntitem.v3, mntitem.v4, mntitem.v5
							, mntitem.v6, mntitem.v7, mntitem.v8, mntitem.v9
							, mntitem.va, mntitem.vb, mntitem.vc, mntitem.vd, mntitem.ve)
					'''
				self.liststore.clear()
				for r in rows:
					self.liststore.append(r)
#				writeCfg()
		except Exception as e:
			tnow = datetime.now().strftime("%H:%M:%S")
			print(APPNAME, "ERROR refreshing list at", tnow, "\nError:", e)
		if debug:
			print(f"Ready in {time.perf_counter() - tim1:0.4f} sec")
#			tnow = datetime.now().strftime("%H:%M:%S")
#			print(APPNAME, "main list refreshed at", tnow)
		return False # see example at https://pygobject.readthedocs.io/en/latest/guide/threading.html

	def chkNtwStat(self, res, sh, path):
		"""Check availability and mount status of network shared folder."""
		if os.path.ismount(path): return "on"
		## Sometimes most useful functions just don't exist
#		if os.path.exists(rem) and os.path.isdir(rem): return "off"
		n = r"^" + sh + r"(?=\s)"
		m = re.search(n, res, re.MULTILINE|re.DOTALL)
		if m:
#			print(m, n)
			mat = m.group()
			if mat == sh: return "off"
		return "not"

	def setTT(self, st, mch):
		"""Set item tooltip."""
		# s = "yes" if st == "on" else "no" # share status
		s = "yes" if st == "on" else "no" if st == "off" else "unavailable" # share status
		m = "online" if mch == "2" else "offline" if mch == "0" else "firewalled"
		t = "IP: %s\nMAC: %s\nHost: %s\nShare: %s\nLocal: %s\nType: %s\n\nMounted: %s\nMachine: %s\nBrand: %s" \
			% (mntitem.vc, mntitem.vd, mntitem.v1, mntitem.v2, mntitem.v3, mntitem.v6, s, m, mntitem.ve)
		return t

	def itemClicked(self, lst, tpath): # self.list, treepath
		"""Perform action when item is clicked."""
		mdl = lst.get_model() # GtkTreeModel/Sort
		itr = mdl.get_iter(tpath)
		if sortedList:
			itr = mdl.convert_iter_to_child_iter(itr)
			mdl = mdl.get_model()
		[label, host, index, state, mach, tt] = mdl.get(itr, 1, 2, 3, 4, 5, 6)
		if debug:
			print(">>", label, host, index, state, mach, tt)
		isValid = readEntry(index)
#		if mach == "0" and state == "off": # machine is offline, we got no business (for now)
		if mach == "0" and state == "not": # machine is offline, we got no business (for now)
			if mntitem.vd == "": # we should also validate the MAC
				MsgBoxErr(self, "Error", "Database has no MAC for " + host)
				self.unclick(lst, tpath)
				return
			r = MsgBoxQst(self, "Mount attempt", "Do you want to wake up the machine?", mntitem.v1 + " is offline")
			if r == Gtk.ResponseType.YES:
				if isValid:
					# send a magic packet (if we have the MAC)
					# then check for machine ready and mount the share.
					self.wakeUp(mntitem.vc, mntitem.vd)
					## add a time-limited entry to a list of wakable machines
					## containing share name to be mounted
					cmnd = self.makeCmnd()
					self.addState(mntitem.v3, True, cmnd, retries, index)
				else:
					print("ERROR: database has no MAC for", host)
			self.unclick(lst, tpath)
			return
		# isMount = False if state == "on" or state == "not" else True
		isMount = True if state == "off" else False
		if isMount:
			cmnd = self.makeCmnd()
		elif state == "not" and mach != "0":
			self.unclick(lst, tpath)
			return
		else:
			## https://phoenixnap.com/kb/linux-mount-command
			##check if mount point is currently in use before attempting to unmount:
			## fuser -m <mount point>
			## if in use show messagebox and allow user to choose -l (lazy unmount)
			## or -f (force unmount), or just abort
			cmnd = "fuser -m " + mntitem.v3 + " | sed 's/^\/.*:\s*\(.*\)/\\1/'"
			r = subprocess.check_output(cmnd, shell=True).decode()
			if debug:
				print(r)
			if r == "":
				## modified to default -l since it can lock up
				## when machine is no longer online
				cmnd = "sudo umount -l " + mntitem.v3
			else:
				opxb = mdl.get_value(itr, 0)
				pxb = PICOMBY
				mdl.set_value(itr, 0, pxb)
				chk = self.MsgBox1("Unmounting", umntMsg)
				if chk == 3 or chk < 0:	# Cancel or close dialog
					mdl.set_value(itr, 0, opxb)
					self.unclick(lst, tpath)
					return
				if chk == 2:	# Force
					cmnd = "sudo umount -f " + mntitem.v3
				elif chk == 1:	# Lazy
					cmnd = "sudo umount -l " + mntitem.v3
				else:
					print("Something else occured; chk =", str(chk))
					self.unclick(lst, tpath)
					return
				self.addState(mntitem.v3, False, cmnd, retries, index)
		r = 0
		if debug:
			print(cmnd)
		else:
			try:
				r = subprocess.check_output(cmnd, shell=True)
			except Exception as e:
				print("Drivemapper error on %s:\n%s" % (cmnd, e))
		if not r:
			s = "on" if isMount else "off"
			pxb = PICOYES if isMount else PICOOFF if mach == "0" else \
				PICOPRO if mach == "1" else PICONO
			tt = self.setTT(s, mach)
			row = tpath[0]
			if debug:
				print("current row:", row)
			mdl.set(itr, 0, pxb, 1, label, 2, host, 3, index, 4, s, 5, mach, 6, tt)
			mdl.row_changed(row, itr)
		else:
			## notification on error (with mount/umount return code)
			ttl = "Mount" if isMount else "Unmount"
			MsgBoxErr(self, ttl, "Operation failed with code " + str(r))
		if debug:
			state = "on" if isMount else "off"
			print("Share", row+1, "is", state) # we count rows from zero
		self.unclick(lst, tpath)

	def unclick(self, l, p): # list, tpath
		"""Remove selection style from item after click."""
		if click1:
			time.sleep(0.15)
			l.unselect_path(p)

	def wakeUp(self, ip, mac):
		"""Send WakeOnLAN signal."""
		cip = ""
		if ip != "":
			i = ip.split('.')
			cip = ".".join([i[0], i[1], i[2], "255"])
		try:
			bcast = " -d " + cip + " " if cip != "" else " "
#		if debug:
			print("1. WOL to", bcast, "MAC", mac)
			subprocess.check_output("pywake -4" + bcast + mac, shell=True)
		except Exception:
			print("1. FAILED WOL to MAC", mac)
			try:
				bcast = " -i " + cip + " " if cip != "" else " "
#			if debug:
				print("2. WOL to", bcast, "MAC", mac)
				subprocess.check_output("wakeonlan" + bcast + mac, shell=True)
			except Exception:
				print("2. FAILED WOL to MAC", mac)
				try:
					print("3. alternative WOL to", ip, "MAC", mac)
					if cip:
						wake1("ip_address=" + cip, mac)
					else:
						wake1(mac)
				except Exception:
					print("3. FAILED all WOL attempts to MAC", mac)

	def makeCmnd(self):
		"""Build mount command for selected item."""
		a = "-v -t " + mntitem.v6 if mntitem.v6 == "vfat" else "-t " + mntitem.v6
		h = mntitem.vc if prefIp else mntitem.v1 + "/" + mntitem.v2
		u = "" if mntitem.v4 == "" else "username=" + mntitem.v4 + ","
		p = "password=," if mntitem.v5 == "" else "password=" + mntitem.v5 + ","
		m = ",umask=" + mntitem.va + "," if mntitem.va != "" else \
			",file_mode=" + mntitem.v8 + ",dir_mode=" + mntitem.v9
		v = "" if mntitem.v7 == "" else ",vers=" + mntitem.v7
		s = "" if mntitem.vb == "" else ",sec=" + mntitem.vb
		o = ",uid=" + USER + ",gid=" + GROUP
		x = ",_netdev,nofail"  # don't add ',noatime' as it throws an error
		return "sudo mount " + a + " //" + h + " " + mntitem.v3 \
			+ " -o " + u + p + "iocharset=utf8" + m + v + s + o + x

	def mountGrp(self, wake=False):
		"""Mount all items."""
		if debug:
			print("Mount all. Active hosts:\n", self.ahosts.items())
		failed = ""
		for s in appcfg.sections():
			if s == "Preferences" or not readEntry(s):
				continue
			cmnd = self.makeCmnd()
			if wake and mntitem.vc != "" and mntitem.vd != "":
				self.wakeUp(mntitem.vc, mntitem.vd)
				self.addState(mntitem.v3, True, cmnd, retries, s)
			elif not wake:
				for n, i in self.ahosts.items():
					if mntitem.v1 == i.name:
						try:
							r = subprocess.check_output(cmnd, shell=True)
							if r:
								failed += "Sect. " + s + ", error " + r + "\n\tCmnd: " + cmnd + "\n"
						except Exception:
							pass
						break
		if len(failed) > 0:
			print(failed)
		self.timerUpdate()

	def unmountGrp(self, force=False):
		"""Unmount all items."""
		if debug:
			print("Unmount all. Active hosts:\n", self.ahosts.items())
		for s in appcfg.sections():
			if s == "Preferences" or not readEntry(s) or mntitem.v3 == "":
				continue
			mnt = False
			for n, i in self.ahosts.items():
				if mntitem.v1 == i.name:
					mnt = True
					break
			if not mnt:
				continue
			t = "-f " if force else "-l "
			cmnd = "sudo umount " + t + mntitem.v3
			self.addState(mntitem.v3, False, cmnd, retries, s)
#		self.timerUpdate() # already done in addState routine

## Context menu in main list
	def entryDetails(self, w, s):
		"""Show Details window for selected item (TBD)"""
		print("Details window for", s, "to be done...")
		self.popup.destroy() # or menu.destroy()

	def entryEdit(self, w, s):
		"""Edit selected item."""
		if debug:
			print("open Edit window for row", s)
		else:
			readEntry(s)
			EditWindow(self, s, 1)
		self.popup.destroy() # or menu.destroy()

	def entryRemove(self, w, s):
		"""Remove selected item from list and configuration."""
		readEntry(s)
		# we need a delete confirmation here, it's too risky
		r = MsgBoxQst(self, "Confirmation",
			# rementMsg % (mntitem.v0, mntitem.v1, mntitem.v2, mntitem.v3),
			"Are you sure you want to remove the entry <b>" \
			+ mntitem.v0 + "</b> for\n<b>//" + mntitem.v1 + "/" + mntitem.v2 \
			+ "</b> mounted at <b>" + mntitem.v3 + "</b> ?",
			"There is no undo for this operation!")
		if r != Gtk.ResponseType.YES:
			return
		if debug:
			print("entryRemove(): remove section", s)
		else:
			appcfg.remove_section(s)
			writeCfg()
			print("entry %s deleted?" % s)
		self.timerUpdate()
		self.popup.destroy() # or menu.destroy()

	def showPopup(self, widget, event, section=None):
		"""Show context menu for selected item."""
		if debug:
			print("popup menu triggered for entry", section)
		self.popup = Gtk.Menu()
		if section is not None:
			cmi = Gtk.MenuItem.new_with_label("Details")
			cmi.connect("activate", self.entryDetails, section)
			pmi = Gtk.MenuItem.new_with_label("Edit")
			pmi.connect("activate", self.entryEdit, section)
			sep1 = Gtk.SeparatorMenuItem()
			ami = Gtk.MenuItem.new_with_label("Remove")
			ami.connect("activate", self.entryRemove, section)
			sep2 = Gtk.SeparatorMenuItem()
			self.popup.append(cmi)
			self.popup.append(pmi)
			self.popup.append(sep1)
			self.popup.append(ami)
			self.popup.append(sep2)
			cmi.show()
			pmi.show()
			sep1.show()
			ami.show()
			sep2.show()
		emi = Gtk.MenuItem.new_with_label("Exit")
		emi.connect("activate", self.quitApp)
		self.popup.append(emi)
		emi.show()
		if event:
			btn = event.button
			etime = event.time
		else:
			btn = 0
			etime = Gtk.get_current_event_time()
		self.popup.attach_to_widget(self.list, None)
		self.popup.popup(None, None, None, None, btn, etime)

	def itemContext(self, widget, event=None):
		"""Get item data and call show context menu."""
		if event is None: # triggered by hotkey (Shift+F10/Menu)
			self.showPopup(widget, None)
			return True
		if event.triggers_context_menu() and event.type == Gdk.EventType.BUTTON_PRESS:
			## get cursor position relative to the window
			## gdk_widget_get_device_position() is recommended instead
			[x, y] = self.list.get_pointer()
			[tpath, pos] = self.list.get_dest_item_at_pos(x, y)
			if debug:
				print(tpath, pos)
			mdl = self.list.get_model()
			itr = mdl.get_iter(tpath)
			[label, host, index, state, ip] = mdl.get(itr, 1, 2, 3, 4, 5)
			if debug:
				print(label, host, index, state, ip)
			## get real row from tpath -> section
#			section = tpath[0] # this leads to visual/sorted row, not stored row!
			self.showPopup(widget, event, index)
			return True
		return False
## various
	def sizeAllocate(self, widget, rect):
		"""Fit items to the resized window."""
		c = self.list.get_columns()
		if c == 0 or rect.width < 40:
			return
		cols = int(rect.width / (c * 2))
		self.list.set_columns(cols)

	def mainWndEvent(self, wnd, event, data=None):
		"""Retrieve new window dimensions and position after resize/move."""
		global change, mainWidth, mainHeight, mainX, mainY
#		if event.type = Gdk.EventType.CONFIGURE:
		mainWidth = event.width
		mainHeight = event.height
		mainX = event.x
		mainY = event.y
		change = True
		return False

## MAIN MENU OPTIONS
	def debugmode(self, item):
		"""Toggle debug mode on/off and switch config file paths."""
		global debug, change, CONFIG
		if change is True:
			writeCfg() # save any current changes when switching debug mode
			change = False
		debug = not debug
		if not debug:
			CONFIG = os.path.join(cfgpath, "config.ini")
			os.makedirs(cfgpath, exist_ok=True)
		else:
			CONFIG = os.path.join(CD, "config.ini")
		appcfg.read(CONFIG)
		self.timerUpdate()
		print(CONFIG, "\ndebug=", debug)

	def timermode(self, item):
		"""Switch periodic updating on/off."""
		global timerChk
		timerChk = not timerChk
		if timerChk:
			self.th = continuous_threading.PeriodicThread(timerInt*60, self.timerUpdate)
			self.th.start()
		elif hasattr(self, 'th'):
			self.th.join()
			self.th.stop()
			self.th.close()
			del self.th
		if debug:
			print("Continuous scan", timerChk)

	def addState(self, share, state, cmnd="", retry=0, idx=0):
		"""Add item state to group command."""
		if share in pending.keys():
			return
		job = lambda: None
		job.name		= share
		job.cmnd		= cmnd
		job.state		= state
		job.retry		= retry
		job.idx			= idx
		pending[share]	= job
		if not hasattr(self, 'stateTimer'):
			self.stateTimer = continuous_threading.PeriodicThread(30, self.doState)
			self.stateTimer.start()

	def doState(self):
		"""Create state checking thread."""
		GLib.idle_add(lambda: self.chkState())

	def chkState(self):
		"""Run state checking thread."""
		global notif
#		for loc in list(pending.keys()): # won't work due to runtime error:
#			if pending[loc].retry == 0: # dictionary changed size during iteration
		for loc in [loc for loc, job in pending.items() if job.retry == 0]:
			del pending[loc]
		if len(pending) == 0:
			self.stateTimer.join()
			self.stateTimer.stop()
			self.stateTimer.close()
			del self.stateTimer
			## In case we have more shares on formerly sleeping machine(s)
			## in the list we have to refresh it to show the machine(s) as online.
			self.updateList()
			return
		print(list(pending.keys()))
		## we have local name and state, to check
		for loc, job in pending.items():
			if job.cmnd != "":
				if job.retry > 0:
					r = job.retry - 1
					print("try %d / %d for %s" % (retries-r, retries, loc))
					try:
						os.system(job.cmnd)
					except Exception:
						pass
#					pending[loc] = job
					pending[loc].retry = r
				else: ## revert the icon to former state (did we store it?)
					## but take into acount machine state change (off/on if it was WOL)
#					pending.pop(loc)
					continue
			s = bool(job.state)
			if os.path.isdir(job.name) and os.path.ismount(job.name) == s: # we got desired state
				## delete item from the dict on next call
				pending[loc].retry = 0
				## update icon in main list to reflect the change until next update
				self.fixIcon(job.idx, s)
				## if enabled send notification too
				try:
					body = job.name + " has been " + ("mounted" if s else "unmounted")
					snd = "device-added" if s else "device-removed"
					notif.update("Info", body, snd)
					notif.show()
				except Exception:
					pass
			elif pending[loc].retry == 0:
				## check machine state, share state, and set corresponding icon
				self.fixIcon(job.idx, os.path.ismount(job.name))
				try:
					body=job.name + " has NOT been " + ("mounted" if s else "unmounted")
					notif.update("Error", body, "dialog-error")
					notif.show()
				except Exception:
					pass

	def fixIcon(self, i, st): # config index, current state
		"""Display correct icon for item in list."""
		mdl = self.list.get_model() # GtkTreeModel/Sort
		if sortedList:
			mdl = mdl.get_model()
		itr = mdl.get_iter_first()
		while itr:
			[label, host, index, state, mach, tt] = mdl.get(itr, 1, 2, 3, 4, 5, 6)
			if index == i:
				## find current machine state, share state
				s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
				s.settimeout(0.5)
				try:
					s.connect((host, 445))
					r = True
				except Exception:
					r = False
				s.close()
				## set proper icon and states
				pxb = PICOYES if st == True else PICONO if r else PICOOFF
				state = "on" if st == True else "off"
				mach = "2" if r else "0"
				isValid = readEntry(index)
				mdl.set(itr, 0, pxb, 4, state, 5, mach, 6, self.setTT(state, mach))
				break
			itr = mdl.iter_next(itr)


## ADDITIONAL WINDOWS
	def configure(self, item):
		"""Open Configuration window."""
		ConfWindow(self)

	def preferences(self, item):
		"""Open Preferences window."""
		PrefWindow(self)

	def about(self, item):
		"""Open About window."""
		About(self)

## Unmount choice message
	def MsgBox1(self, title, msg):
		"""MessageBox question for unmounting choice."""
		mdlg = Gtk.MessageDialog(
			transient_for=self,
			modal=True,
			message_type=Gtk.MessageType.QUESTION,
			buttons=Gtk.ButtonsType.NONE,
			title=APPNAME + " - " + title,
			text="User choice needed"
		)
		mdlg.format_secondary_markup(msg)
		mdlg.add_buttons("Lazy", 1, "Force", 2, "Cancel", 3)
		img = Gtk.Image.new_from_icon_name(ICOHLP,iLSz)
		img.show()
		mdlg.set_image(img) # Deprecated but still works!
		res = mdlg.run()
		mdlg.destroy()
		return res

## https://programtalk.com/vs4/python/13919/screenkey/
	def setSystray(self):
		"""Choose tray indicator method."""
		icopath = os.path.abspath(os.path.dirname(__file__))
		Gtk.IconTheme.get_default().append_search_path(icopath)
		if useAppInd:
			self.useAppInd()
		else:
			self.useGtkStsIcn()

	def useAppInd(self):
		"""Try to enable and use AppIndicator."""
		try:
			gi.require_version('AppIndicator3', '0.1')
			from gi.repository import AppIndicator3 as appindicator
			self.systray = appindicator.Indicator.new(
			APPNAME, 'drive-mapper', appindicator.IndicatorCategory.APPLICATION_STATUS)
			self.systray.set_status(appindicator.IndicatorStatus.ACTIVE) # PASSIVE/ACTIVE/ATTENTION
			self.systray.set_attention_icon_full("drive-mapper", APPNAME)
			self.systray.set_icon_full("drive-mapper", APPNAME)
			self.systray.set_label(APPNAME + " " + APPVERSION, '') # doesn't work in Mint
			self.systray.set_title(APPNAME + " " + APPVERSION)
			self.systray.set_secondary_activate_target(self.cmi) # doesn't work
			self.systray.set_menu(self.menutray)
			self.systray.connect("scroll-event", self.systrayActive)
			print("useAppInd(): Using AppIndicator.")
		except ImportError:
			print("useAppInd(): AppIndicator not available, falling back to Gtk.StatusIcon.")
			self.useGtkStsIcn()

	def useGtkStsIcn(self):
		"""Enable GtkStatusIcon."""
		self.systray = Gtk.StatusIcon()
		self.systray.set_from_icon_name("drive-mapper")
		self.systray.set_tooltip_markup(APPNAME + " " + APPVERSION)
		self.systray.connect("popup-menu", self.trayPopup)
		self.systray.connect("activate", self.systrayActive)
		print("useGtkStsIcn(): Using StatusIcon.")
#		self.systray.set_blinking(True)

	def trayPopup(self, icon, btn, ctime):
		"""Show systray popup menu."""
		if debug:
			print("popup()", icon, btn, ctime)
		self.menutray.popup_at_pointer()

	def hideApp(self, btn):
		"""Hide main application window to tray."""
		self.hide()

## https://www.codeproject.com/articles/27142/minimize-to-tray-with-gtk
	def winState(self, widget, event):
		"""Acknowledge main window state change."""
		if (event.changed_mask == Gdk.WindowState.ICONIFIED) and \
			(event.new_window_state == Gdk.WindowState.ICONIFIED or \
			event.new_window_state == (Gdk.WindowState.ICONIFIED | Gdk.WindowState.MAXIMIZED)):
			self.hide()
			print("winState(): hidden")
		elif (event.changed_mask == Gdk.WindowState.WITHDRAWN) and \
			(event.new_window_state == Gdk.WindowState.ICONIFIED or \
			event.new_window_state == (Gdk.WindowState.ICONIFIED | Gdk.WindowState.MAXIMIZED)):
			print("winState(): restored")
		return True

	def systrayActive(self, sysicon, d1=None, d2=None):
		"""Show/hide main window on systray icon scroll."""
		if d2 == Gdk.ScrollDirection.SMOOTH:
			return
		if not self.is_visible():
			self.deiconify() # restore the hidden window
			# we should fix position coordinates before showing window
			self.present() # show the window
		else:
			self.hide()
		self.otherWin()
		if debug:
			print("systrayActive()", d1, d2)

	def otherWin(self):
		"""Show/hide other child windows if open."""
#		global ConfWin, PrefWin, EditWin, AboutWin
		wl = [ConfWin, PrefWin, EditWin, AboutWin]
		for w in wl:
			if w is not None:
				if not w.is_visible():
					w.deiconify() # restore the hidden window
					w.present() # show the window
				else:
					w.hide()

	def quitApp(self, par):
		if hasattr(self, 'th'):
			self.th.join()
			self.th.stop()
			self.th.close()
		try:
			Notify.uninit()
			subprocess.run("sudo pkill -f notification-daemon &", shell=True) # use '-f' to force kill
			# subprocess.check_output("sudo pkill -f notification-daemon &", shell=True) # use '-f' to force kill
		except Exception:
			pass
		try:
			notif.uninit()
		except Exception:
			pass
		if change is True or not appcfg.has_section("Preferences"):
			writePreferences()
		app.quit()
##===============================================================
##	CONFIGURATION WINDOW
##===============================================================
class ConfWindow(Gtk.ApplicationWindow):
	"""Configuration window."""
	def __init__(self, wnd):
		super(ConfWindow, self).__init__(title=APPNAME + " - Configuration", application=app)
		global ConfWin
		self.wnd = wnd
		self.set_modal(True)
#		self.set_transient_for(wnd)
		self.set_transient_for(app.win)
		self.set_skip_taskbar_hint(True)
		self.set_icon(mainIcon)
		self.maingrid = Gtk.Grid(halign=Gtk.Align.START, valign=Gtk.Align.START
			, margin = 5, expand=True, column_spacing = 5, row_spacing = 5)
		listbox = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL
			, hexpand=False, vexpand=False, halign=Gtk.Align.FILL, valign=Gtk.Align.FILL)
		grid1 = Gtk.Grid(halign=Gtk.Align.FILL, valign=Gtk.Align.FILL
			, margin = 1, expand=True, column_spacing = 5, row_spacing = 5)
		l1 = Gtk.Label(label="<b>Local IP(s)</b>", halign=Gtk.Align.CENTER, hexpand=False, use_markup=True)
		l2 = Gtk.Label(label="<b>Hosts</b>", halign=Gtk.Align.CENTER, hexpand=False, use_markup=True)
		l3 = Gtk.Label(label="<b>Share names</b>", halign=Gtk.Align.CENTER, hexpand=False, use_markup=True)
		l4 = Gtk.Label(label="<b>Status:</b>", halign=Gtk.Align.CENTER, hexpand=False, use_markup=True)
		l5 = Gtk.Label(label="<b>Item:</b>", halign=Gtk.Align.CENTER, hexpand=False, use_markup=True)
		self.list1 = Gtk.ListBox(margin=4)
		f1 = Gtk.Frame(hexpand=False)
		f1.set_label_widget(l1)
		f1.set_label_align(0.5, 0.5)
		f1.add(self.list1)
		self.list1.set_selection_mode(Gtk.SelectionMode.SINGLE)
		self.list1.set_activate_on_single_click(True)
		self.list1.connect('row-activated', self.selectIP)
		self.list2 = Gtk.ListBox(margin=4)
		f2 = Gtk.Frame(hexpand=False)
		f2.set_label_widget(l2)
		f2.set_label_align(0.5, 0.5)
		f2.add(self.list2)
		self.list2.set_selection_mode(Gtk.SelectionMode.SINGLE)
		self.list2.set_activate_on_single_click(True)
		self.list2.connect('row-activated', self.selectHost)
		self.list3 = Gtk.ListBox(margin=4)
		f3 = Gtk.Frame(hexpand=False)
		f3.set_label_widget(l3)
		f3.set_label_align(0.5, 0.5)
		f3.add(self.list3)
		self.list3.set_selection_mode(Gtk.SelectionMode.SINGLE)
		self.list3.set_activate_on_single_click(True)
		self.list3.connect('row-activated', self.getInfo)
		grid1.attach(f1, 0, 0, 1, 1)
		grid1.attach(f2, 1, 0, 1, 1)
		grid1.attach(f3, 2, 0, 1, 1)
		## item labels and action buttons
		grid2 = Gtk.Grid(halign=Gtk.Align.START, valign=Gtk.Align.START
			, margin = 1, expand=True, column_spacing = 5, row_spacing = 5)
		grid2.attach(l4, 0, 0, 1, 1)
		stsbox = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, hexpand=False, vexpand=False)
		## label 'Status'
		self.statuslbl = Gtk.Label(label="", halign=Gtk.Align.CENTER, hexpand=False, margin=2)
		stsbox.pack_start(self.statuslbl, True, True, 0)
		## label 'Listed'
		self.listedlbl = Gtk.Label(label="", halign=Gtk.Align.CENTER, hexpand=False, margin=2)
		stsbox.pack_start(self.listedlbl, True, True, 0)
		## label 'Mounted'
		self.mountedlbl = Gtk.Label(label="", halign=Gtk.Align.CENTER, hexpand=False, margin=2)
		stsbox.pack_start(self.mountedlbl, True, True, 0)
		## label 'Type'
		self.typelbl = Gtk.Label(label="", halign=Gtk.Align.CENTER, hexpand=False, margin=2)
		stsbox.pack_start(self.typelbl, True, True, 0)
		grid2.attach(stsbox, 1, 0, 1, 1)
		grid2.attach(l5, 0, 1, 1, 1)
		btnbox = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, hexpand=False, vexpand=False)
		## button 'New'
		self.btnnew = Gtk.Button(label="New", halign=Gtk.Align.START, hexpand=False, margin=2)
		self.btnnew.set_always_show_image(True)
		img = Gtk.Image.new_from_icon_name(ICONEW, iSSz)
		self.btnnew.set_image(img)
		self.btnnew.connect('clicked', self.mountNew)
		btnbox.pack_start(self.btnnew, True, True, 0)
		## button 'Add'
		self.btnadd = Gtk.Button(label="Add", halign=Gtk.Align.START, hexpand=False, margin=2)
		self.btnadd.set_always_show_image(True)
		img = Gtk.Image.new_from_icon_name(ICOADD, iSSz)
		self.btnadd.set_image(img)
		self.btnadd.connect('clicked', self.mountAdd)
		btnbox.pack_start(self.btnadd, True, True, 0)
		## button 'Edit'
		self.btnedt = Gtk.Button(label="Edit", halign=Gtk.Align.START, hexpand=False, margin=2)
		self.btnedt.set_always_show_image(True)
		img = Gtk.Image.new_from_icon_name(ICOEDT, iSSz)
		self.btnedt.set_image(img)
		self.btnedt.connect('clicked', self.mountEdit)
		btnbox.pack_start(self.btnedt, True, True, 0)
		## button 'Remove'
		self.btnrmv = Gtk.Button(label="Remove", halign=Gtk.Align.START, hexpand=False, margin=2)
		self.btnrmv.set_always_show_image(True)
		img = Gtk.Image.new_from_icon_name(ICORMV, iSSz)
		self.btnrmv.set_image(img)
		self.btnrmv.connect('clicked', self.mountRemove)
		btnbox.pack_start(self.btnrmv, True, True, 0)
		grid2.attach(btnbox, 1, 1, 1, 1)
		self.resetBtns()
		## scrolled window
		listbox.add(grid1)
		self.maingrid.attach(listbox, 0, 0, 4, 1) # left,top,width,height
		self.maingrid.attach(grid2, 0, 1, 4, 1) # left,top,width,height
		## buton 'Rescan'
		btnrsc = Gtk.Button(label="Rescan network", halign=Gtk.Align.FILL, hexpand=False)
		btnrsc.set_always_show_image(True)
		img = Gtk.Image.new_from_icon_name(ICORSC, iNSz)
		btnrsc.set_image(img)
		btnrsc.connect('clicked', self.getIPs)
		self.maingrid.attach(btnrsc, 0, 2, 1, 1)
		## buton 'OK'
		btnok = Gtk.Button(label="OK", halign=Gtk.Align.FILL, hexpand=False)
		btnok.set_always_show_image(True)
		img = Gtk.Image.new_from_icon_name(ICOOK, iNSz)
		btnok.set_image(img)
		btnok.connect('clicked', self.accept, self.wnd)
		self.maingrid.attach(btnok, 2, 2, 1, 1)
		## button 'Cancel'
		btnccl = Gtk.Button(label="Cancel", halign=Gtk.Align.FILL, hexpand=False)
		btnccl.set_always_show_image(True)
		img = Gtk.Image.new_from_icon_name(ICOCCL, iNSz)
		btnccl.set_image(img)
		btnccl.connect('clicked', self.exit)
		self.maingrid.attach(btnccl, 3, 2, 1, 1)
		self.add(self.maingrid)
		self.itemtype = None
		self.getIPs()
		self.show_all()
		self.list1.unselect_all()
		hints = Gdk.Geometry()
		hints.min_width = 250
		hints.min_height = 135
		mask = Gdk.WindowHints.MIN_SIZE
		self.set_geometry_hints(None, hints, mask)
		ConfWin = self

	def getIPs(self, btn=None):
		"""Build list of IPs."""
		self.clearList(self.list1)
		self.clearList(self.list2)
		self.clearList(self.list3)
		self.clearStatus()
		res = subprocess.check_output("hostname -I", shell=True).decode().rstrip()
		if debug:
			print("[" + res + "]")
		n = res.split(' ')
		for x, i in enumerate(n):
			row = Gtk.ListBoxRow()
			row.data = i
			row.add(Gtk.Label(label=i, halign=Gtk.Align.START))
			self.list1.add(row)
		self.list1.show_all()

	def selectIP(self, ilist, item, data=None):
		"""Select IP."""
		self.clearList(self.list2)
		self.clearList(self.list3)
		self.clearStatus()
		data = item.data
		i = data.split(".")
		ip = ".".join([i[0], i[1], i[2], "*"])
		if debug:
			print("Check IP range", ip)
			tim1 = time.perf_counter()
		cmnd = nmapCmnd + " " + ip + " | sed " + replex + " | sed 's/[()]//g' | sort"
		res = subprocess.check_output(cmnd, shell=True).decode()
		if debug:
			print("[" + res + "]")
			print(f"Scan ready in {time.perf_counter() - tim1:0.4f} sec")
		n = res.split('\n')
		for x, j in enumerate(n):
			if j == "" or ":" not in j: # check for bad host (missing MAC)
				continue
			m = j.split('\t')
			if debug:
				print(x, m[0], m[1], m[2])
			row = Gtk.ListBoxRow()
			g = m[0].split(' ')
			try:
				row.data = g[0]
				row.ip = g[1]
			except Exception:
				row.data = "Unknown"
				row.ip = g[0]
				m[0] = row.data + " (" + m[0] + ")"
			row.mac = m[1]
			row.brd = m[2]
			row.avail = "Y"
			row.add(Gtk.Label(label=m[0], halign=Gtk.Align.START))
			self.list2.add(row)
		self.list2.show_all()
		self.chkWinSz()

	def selectHost(self, ilist, item, data=None):
		"""Select host."""
		self.clearList(self.list3)
		self.clearStatus()
		self.resetBtns()
		host = item.data if item.data != "Unknown" and item.data != "" else item.ip
		if debug:
			print(host)
		cmnd = "smbclient -U \"guest%\" -L //" + host + " | sed " + replsh + " | sort"
		res = subprocess.check_output(cmnd, shell=True).decode()
		if debug:
			print("[" + res + "]")
		n = res.split('\n')
		for x, j in enumerate(n):
			if j == "":
				continue
			j = j.strip()
			if debug:
				print(x, "[" + j + "]")
			row = Gtk.ListBoxRow()
			row.data = j
			row.ip = item.ip
			row.mac = item.mac
			row.brd = item.brd
			row.avail = "Y"
			row.add(Gtk.Label(label=j, halign=Gtk.Align.START))
			self.list3.add(row)
		self.list3.show_all()
		self.chkWinSz()

	def getInfo(self, ilist, item, data=None):
		"""Get info on selected share."""
		host = self.list2.get_selected_row()
		server = str(host.data)
		share = str(item.data)
		listed = local = self.itemtype = ""
		avail = host.avail == "Y" and item.avail == "Y"
		i = 1
		while True:
			res = readEntry(i)
			if not res:
				break
			if debug:
				print("Listed", mntitem.v0, "=>", mntitem.v1, mntitem.v2, mntitem.v3)
			if mntitem.v1 == server and mntitem.v2 == share:
				listed = mntitem.v3
				break
			i += 1
		## check all local mounts and find out if any equals to this share
		## mount is a very good candidate //<server>/<share> on /<local> type <type> <opts>
		## call mount -t <type> to filter only that type
		cmnd = "mount | grep // | sed " + replmt + " | sort"
		r = subprocess.check_output(cmnd, shell=True).decode()
		m = r.split('\n')
		for l in m:
			e = l.split('\t')
			s = e[0].split('/')
			if s[0] == server and s[1] == share:
				local = e[1]
				self.itemtype = e[2] # better make it mntitem.v6
				# split e[3] by comma and retrieve the rest of the info
				o = e[3].split(',')
				break
		mntitem.v1 = mntitem.v1 if mntitem.v1 != "" else server
		mntitem.v2 = mntitem.v2 if mntitem.v2 != "" else share
		mntitem.v3 = listed if not local else local
		mntitem.v6 = self.itemtype if self.itemtype != "" else ""
		mntitem.vc = item.ip
		mntitem.vd = item.mac
		mntitem.ve = item.brd
		lbl1 = "<span foreground='blue'><b>Available</b></span>" if avail \
			else "<span foreground='red'>Offline</span>"
		lbl2 = "<span foreground='blue'><b>Listed</b></span>" if listed != "" \
			else "<span foreground='red'>Unlisted</span>"
		lbl3 = "<span foreground='blue'><b>Mounted</b></span>" if os.path.ismount(mntitem.v3) \
			else "<span foreground='red'>Unmounted</span>"
		lbl4 = "[" + mntitem.v6 + "]" if mntitem.v6 != "" else ""
		if debug:
			print("Selected server=", mntitem.v1, "share=", mntitem.v2, "local=", mntitem.v3)
		self.statuslbl.set_markup(lbl1)
		self.listedlbl.set_markup(lbl2)
		self.mountedlbl.set_markup(lbl3)
		self.typelbl.set_markup(lbl4)
		if avail and not listed:
			self.resetBtns(True)
		elif listed != "":
			self.resetBtns(False, True, True)
		self.show_all()

	def mountNew(self, btn):
		"""Add new mount."""
		s = 1
		while True:
			if appcfg.has_section(str(s)):
				s += 1
				continue
			break
		if debug:
			print("new section", s)
		readEntry(s) # just to blank out mntitem values
		EditWindow(self, s)
		print("exit mountNew() with change=%s" % change)

	def mountAdd(self, btn):
		"""Add selected mount."""
		s = 1
		while True:
			if appcfg.has_section(str(s)):
				s += 1
				continue
			break
		## show add dialog
		if debug:
			print("adding section", s, "for //", mntitem.v1, mntitem.v2, "type", mntitem.v6)
		EditWindow(self, s)
		self.getInfo(self.list3, self.list3.get_selected_row())
		print("exit mountAdd() with change=%s" % change)

	def mountEdit(self, btn):
		"""Edit selected mount."""
		server = self.list2.get_selected_row().data
		share = self.list3.get_selected_row().data
		sect = appcfg.sections()
		for s in sect:
			if s == "Preferences":
				continue
			if not readEntry(s):
				self.mountAdd(btn)
				return
			if mntitem.v1 == server and mntitem.v2 == share:
				## show edit dialog
				if debug:
					print("editing //", mntitem.v1, mntitem.v2)
				EditWindow(self, s, 1)
				break
		self.getInfo(self.list3, self.list3.get_selected_row())
		print("exit mountEdit() with change=%s" % change)

	def mountRemove(self, btn):
		"""Remove selected mount."""
		server = self.list2.get_selected_row().data
		share = self.list3.get_selected_row().data
		sect = appcfg.sections()
		for s in sect:
			if s == "Preferences":
				continue
			if not readEntry(s):
				break
			if mntitem.v1 == server and mntitem.v2 == share:
				## confirmation dialog
				if debug:
					print("deleting entry", s, "for //", server, share)
				r = MsgBoxQst(self, "Confirmation",
# "Are you sure you want to remove the entry for\n<b>//" + server + "/" + share + "</b> ?",
					rement2Msg %(server, share),
					"There is no undo for this operation!")
				if r != Gtk.ResponseType.YES:
					return
				if debug:
					print("appcfg.remove_section(" + s + ")")
				else:
					appcfg.remove_section(s)
				break
		self.getInfo(self.list3, self.list3.get_selected_row())
		print("exit mountRemove() with change=%s" % change)

	def clearList(self, lst):
		"""Clear items list."""
		rows = lst.get_children()
		for r in rows:
			lst.remove(r)

	def clearStatus(self):
		"""Clear status."""
		self.statuslbl.set_markup("")
		self.listedlbl.set_markup("")
		self.mountedlbl.set_markup("")
		self.typelbl.set_markup("")

	def resetBtns(self, a=False, e=False, r=False):
		"""Set buttons sensitivity."""
		self.btnadd.set_sensitive(a)
		self.btnedt.set_sensitive(e)
		self.btnrmv.set_sensitive(r)

	def chkWinSz(self, win=""):
		"""Check and resize window."""
		win = win if win != "" else self
		wm, wn = win.get_preferred_size()
		if debug:
			print("minimum size:", wm.width, "x", wm.height)
			print("natural size:", wn.width, "x", wn.height)
		win.resize(wm.width, wm.height)

	def accept(self, btn, wnd):
		"""Accept changes and close Configuration window."""
		if debug:
			print("writing changes to the config file")
		else:
			## save the changes to config.ini and refresh list
			writeCfg()
			app.win.updateList()
		self.destroy()
		ConfWin = None
		print("exit ConfWin() with change=%s" % change)

	def exit(self, btn, data=None):
		"""Exit Configuration discarding changes."""
		readCfg()
		self.destroy()
		ConfWin = None
##===============================================================
##	ADD / EDIT WINDOW
##===============================================================
class EditWindow(Gtk.ApplicationWindow):
	"""Build Add/Edit window."""
	def __init__(self, wnd, section, t=0):
		super(EditWindow, self).__init__(title=APPNAME + " - Add/edit", application=app)
		global ConfWin, EditWin
		print("change = %s in EditWindow() start" % change)
		self.wnd = wnd
		self.connect('delete-event', self.exit)
		self.set_modal(True)
		self.set_transient_for(self.wnd)
		self.set_skip_taskbar_hint(True)
		self.set_icon(mainIcon)
		mode = "add" if t == 0 else "edit"
		confLbls = ["Listing label:", "Host name/IP", "Share name", "Mount point:", "Username:", "Password:", "FS type:", "File mode:", "Umask:"]
		fList = ["", "auto", "cifs", "ecryptfs", "ecryptfs_private", "exfat", "exfat-fuse", "fuse", "nfs", "nilfs2", "ntfs", "ntfs-3g", "lowntfs-3g", "vfat"]
		vList = ["", "1.0", "2.0", "2.1", "3.0", "3.0.2", "3.1.1"]
		mList = ["0777", "0775", "0770", "0755", "0666", "0660", "0644", "0600"]
		uList = ["0002", "0022", "0077", "0111", "0113"]
		nList = ["ntlm", "ntlm2"]
		self.maingrid = Gtk.Grid(halign=Gtk.Align.FILL, valign=Gtk.Align.FILL
			, margin = 5, expand=True, column_spacing = 5, row_spacing = 5
			, column_homogeneous=True)
		hdrtxt = "new share" if mntitem.v1 == "" else "<b>//" + mntitem.v1 + "/" + mntitem.v2 + "</b>"
		ml = Gtk.Label(label="Select entry options for " + hdrtxt
			, halign=Gtk.Align.CENTER, hexpand=True, use_markup=True)
		self.maingrid.attach(ml, 0, 0, 2, 1)
		self.grid = Gtk.Grid(halign=Gtk.Align.FILL, valign=Gtk.Align.FILL
			, margin = 5, expand=True, column_spacing = 5, row_spacing = 5)
		for i, l in enumerate(confLbls):
			self.grid.attach(Gtk.Label(label=l, halign=Gtk.Align.END), 0, i+1, 1, 1)
		## label
		self.cLabel = Gtk.Entry()
		self.cLabel.grab_focus_without_selecting()
		x = mntitem.v0 if mntitem.v0 != "" else mntitem.v1 + " " + mntitem.v2
		self.cLabel.set_text(x)
		## host
		self.ip = ""
		if mntitem.v1 != "" and ConfWin is not None:
			try:
				self.ip = ConfWin.list2.get_selected_row().ip
			except Exception:
				self.ip = mntitem.vc
		else:
			self.ip = mntitem.vc
		self.cHost = Gtk.ComboBoxText.new_with_entry()
		if mntitem.v1 != "":
			self.cHost.append_text(mntitem.v1)
		if self.ip != "":
			self.cHost.append_text(self.ip)
		self.cHost.set_active(1 if prefLstIp and self.ip != "" else 0)
		## share
		self.cShare = Gtk.Entry()
		self.cShare.grab_focus_without_selecting()
		x = self.inSpace(mntitem.v2) if mntitem.v2 != "" else ""
		self.cShare.set_text(x)
		## mount
		self.cMount = Gtk.FileChooserButton("Select the mount point for mounting " + mntitem.v2, action=Gtk.FileChooserAction.SELECT_FOLDER)
		x = mntitem.v3 if mntitem.v3 != "" else "/media"
		self.cMount.set_current_folder(x)
		self.cMount.set_filename(x)
		self.cMount.set_show_hidden(False)
		# this makes 'Other...' dissapear for unmounted shares
#		self.cMount.set_local_only(True)
		self.cMount.set_create_folders(True)
		ffilter = Gtk.FileFilter()
		ffilter.add_mime_type("inode/directory")
		ffilter.set_name("Folders")
		self.cMount.add_filter(ffilter)
		self.cMount.connect("file-set", self.fldSel)
		##  username
		self.cUser = Gtk.ComboBoxText.new_with_entry()
		self.cUser.append_text("")
		self.cUser.set_active(0)
		## crawl all username entries in the ini
#		inlist = False
		sect = appcfg.sections()
		users = []
		for s in sect:
			if s == "Preferences":
				continue
			u = appcfg.get(s, "user")
			if u != "" and u not in users:
				users.append(u)
		if mntitem.v4 != "" and mntitem.v4 not in users:
			users.append(mntitem.v4)
		users.sort()
		for i, u in enumerate(users):
			self.cUser.append_text(u)
			if mntitem.v4 == u:
				self.cUser.set_active(i)
		## password
		self.cPass = Gtk.Entry(hexpand=True)
		self.cPass.set_visibility(False)
		self.cPass.set_invisible_char("●")
		self.cPass.set_input_purpose(Gtk.InputPurpose.PASSWORD)
		self.cPass.set_text(self.inSpace(mntitem.v5))
		cPasRev = Gtk.ToggleButton(label="👁") # pass revealer button 👁 (U+1F441)
		cPasRev.connect("toggled", self.pasVis, self.cPass)
		cPasBox = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, vexpand=False)
		cPasBox.add(self.cPass)
		cPasBox.add(cPasRev)
		## sudo mount -v -t vfat ???
		## CIFS may also require domain=<workgroup> , check out sec=ntlm
		## add UID/GID for file ownership (uid=1000, gid=1000) or
		## uid=$(id -u <user>) & gid=1$(id -g <user>)
		## cifs, NFS, SSHFS, ocfs2, Ceph, OpenAFS, GlusterFS, CODA
		## share type
		self.cType = Gtk.ComboBoxText() # list all common file system types
		for i, x in enumerate(fList):
			self.cType.append_text(x)
			if x == mntitem.v6:
				self.cType.set_active(i)
		## SMB version
		self.cVers = Gtk.ComboBoxText() # list all cifs versions
		for i, x in enumerate(vList):
			self.cVers.append_text(x)
			if x == mntitem.v7:
				self.cVers.set_active(i)
		cFSBox = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, vexpand=False)
		cFSBox.add(self.cType)
		cFSBox.add(Gtk.Label(label="SMB:", halign=Gtk.Align.END, hexpand=True, margin_right=5))
		cFSBox.add(self.cVers)
		## file/dir mode
		## one might need umask=0700 instead of these, but not for cifs. works with exfat
		self.cFmode = Gtk.ComboBoxText.new_with_entry() # common file attributes
		self.cDmode = Gtk.ComboBoxText.new_with_entry() # common folder attributes
		e = self.cFmode.get_child()
		e.set_max_length(30)
		e.set_width_chars(5)
		e.set_max_width_chars(5)
		e = self.cDmode.get_child()
		e.set_max_length(30)
		e.set_width_chars(5)
		e.set_max_width_chars(5)
		for i, x in enumerate(mList):
			self.cFmode.append_text(x)
			self.cDmode.append_text(x)
			if x == mntitem.v8:
				self.cFmode.set_active(i)
			if x == mntitem.v9:
				self.cDmode.set_active(i)
		self.cFmode.connect('changed', self.fmodSel)
		self.cDmode.connect('changed', self.dmodSel)
		cModeBox = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, vexpand=False)
		cModeBox.add(self.cFmode)
		cModeBox.add(Gtk.Label(label="Dir mode:", halign=Gtk.Align.END, hexpand=True, margin_left=10, margin_right=5))
		cModeBox.add(self.cDmode)
		## umask/security
		self.cUmode = Gtk.ComboBoxText.new_with_entry()
		self.cSec = Gtk.ComboBoxText.new_with_entry()
		e = self.cUmode.get_child()
		e.set_max_length(30)
		e.set_width_chars(5)
		e.set_max_width_chars(5)
		e = self.cSec.get_child()
		e.set_max_length(50)
		e.set_width_chars(8)
		e.set_max_width_chars(8)
		for i, x in enumerate(uList):
			self.cUmode.append_text(x)
			if x == mntitem.va:
				self.cUmode.set_active(i)
		for i, x in enumerate(nList):
			self.cSec.append_text(x)
			if x == mntitem.vb:
				self.cSec.set_active(i)
		self.cUmode.connect('changed', self.umodSel)
		cMod2Box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, vexpand=False)
		cMod2Box.add(self.cUmode)
		cMod2Box.add(Gtk.Label(label="Security:", halign=Gtk.Align.END, hexpand=True, margin_left=5, margin_right=5))
		cMod2Box.add(self.cSec)
		obj = [self.cLabel, self.cHost, self.cShare, self.cMount, self.cUser, cPasBox, cFSBox, cModeBox, cMod2Box]
		for i, l in enumerate(obj):
			self.grid.attach(l, 1, i+1, 1, 1)
		self.maingrid.attach(self.grid, 0, 1, 2, 1)
		## Add interlocking for conflicting settings
		## main window buttons
		btnok = Gtk.Button(label="OK", halign=Gtk.Align.FILL, expand=True)
		btnok.set_always_show_image(True)
		img = Gtk.Image.new_from_icon_name(ICOOK, iNSz)
		btnok.set_image(img)
		self.maingrid.attach(btnok, 0, 2, 1, 1)
		btnccl = Gtk.Button(label="Cancel", halign=Gtk.Align.FILL, expand=True)
		btnccl.set_always_show_image(True)
		img = Gtk.Image.new_from_icon_name(ICOCCL, iNSz)
		btnccl.set_image(img)
		self.maingrid.attach(btnccl, 1, 2, 1, 1)
		btnok.connect('clicked', self.accept, [mode, section])
		btnccl.connect('clicked', self.exit, [mode, section])
		self.add(self.maingrid)
		self.show_all()
		self.cLabel.grab_focus() # won't help with the imbecils selecting all text by default
		wm, wn = self.get_preferred_size()
		self.resize(wm.width, wm.height)
		hints = Gdk.Geometry()
		hints.min_width = wm.width
		hints.max_width = SCREEN.get_width()
		hints.min_height = wm.height
		hints.max_height = wm.height
		mask = Gdk.WindowHints.MIN_SIZE | Gdk.WindowHints.MAX_SIZE
		self.set_geometry_hints(None, hints, mask)
		EditWin = self

## convert \040 to space
	def inSpace(self, txt):
		r"""Convert \040 to space in password."""
		return re.sub(r"\\040", r" ", txt)
## convert space to \040
	def outSpace(self, txt):
		r"""Convert space to \040 in password."""
		return re.sub("\s", r"\\040", txt)
## mount point
	def fldSel(self, combo):
		"""Folder selection for mount point."""
		i = combo.get_current_folder()
#		j = combo.get_filename()
		if debug:
			print("Mount point changed to", i)
## file mode
	def fmodSel(self, combo):
		"""SMB file mode selection."""
		i = combo.get_active_text()
		if debug:
			print("SMB version changed to", i)
## dir mode
	def dmodSel(self, combo):
		"""SMB directory mode selection."""
		i = combo.get_active_text()
		if debug:
			print("SMB version changed to", i)
## umask
	def umodSel(self, combo):
		"""umask selection."""
		i = combo.get_active_text()
		if debug:
			print("SMB version changed to", i)

	def pasVis(self, btn, pf):
		"""Password field visibility toggle."""
		is_active = btn.get_active()
		pf.set_visibility(is_active)

## dialog buttons OK / Cancel
	def accept(self, btn, data=None):
		"""Accept changes in Add/Edit and close window if no error."""
		forbidden = ["/bin", "/boot", "/dev", "/etc", "/lib", "/opt", "/proc", "/root", "/run", "/sbin", "/srv", "/sys", "/usr", "/var"]
		fld = self.cMount.get_current_folder()
		if fld in ["/", "/home", "/media", "/mnt"]:
#			self.MsgBox(warnMsg, badpathMsg % (fld, self.cShare.get_text()), fbdmntMsg)
			MsgBoxWrn(self, warnMsg, badpathMsg % (fld, self.cShare.get_text()), fbdmntMsg)
			return
		for fp in forbidden:
			if fld.startswith(fp):
#				self.MsgBox(warnMsg, badpathMsg % (fld, self.cShare.get_text()), fbdmntMsg)
				MsgBoxWrn(self, warnMsg, badpathMsg % (fld, self.cShare.get_text()), fbdmntMsg)
				return
		mntitem.v0 = self.cLabel.get_text()
#		mntitem.v1 = self.cHost.get_text()
		mntitem.v1 = self.cHost.get_active_text()
		mntitem.v2 = self.outSpace(self.cShare.get_text())
		mntitem.v3 = self.cMount.get_current_folder()
		mntitem.v4 = self.cUser.get_active_text()
		mntitem.v5 = self.outSpace(self.cPass.get_text())
		mntitem.v6 = self.cType.get_active_text()
		mntitem.v7 = self.cVers.get_active_text()
		mntitem.v8 = self.cFmode.get_active_text()
		mntitem.v9 = self.cDmode.get_active_text()
		mntitem.va = self.cUmode.get_active_text()
		mntitem.vb = self.cSec.get_active_text()
		mntitem.vc = self.ip
#		mntitem.vd = "" #self.mac
#		mntitem.ve = "" #self.brd
		## save the changes to config.ini
		if debug:
			print("finished", data[0], "entry", data[1])
			for i, x in vars(mntitem).items():
				print(i, x)
		else:
			addSection(data[1], data[0] != "add")
			print("%s section %s in EditWindow()" % (data[0], data[1]))
		## we should update the list differently if only
		## an existing entry was modified, or a new one added
		change = True
		app.win.timerUpdate()
		self.destroy()
		EditWin = None
		print("exit EditWindow() on OK with change=%s and EditWin=%s" % (change, EditWin))

	def exit(self, btn, data=None):
		"""Close Add/Edit window discarding changes."""
		if debug:
			print("CANCELED", data[0], "entry", data[1])
		self.destroy()
		EditWin= None
##===============================================================
##	PREFERENCES WINDOW
##===============================================================
class PrefWindow(Gtk.ApplicationWindow):
	"""Preferences window."""
	def __init__(self, wnd):
		super(PrefWindow, self).__init__(title=APPNAME + " - Preferences", application=app)
		global PrefWin
		self.wnd = wnd
		self.set_modal(True)
		self.set_transient_for(self.wnd)
		self.set_skip_taskbar_hint(True)
		self.set_icon(mainIcon)
		self.grid = Gtk.Grid(halign=Gtk.Align.FILL, valign=Gtk.Align.FILL
			, margin = 5, expand=True, column_spacing = 5, row_spacing = 5)
		self.grid.add(Gtk.Label(label="Work in progress...", halign=Gtk.Align.CENTER))
		btnok = Gtk.Button(label="OK", halign=Gtk.Align.FILL, expand=True)
		btnok.set_always_show_image(True)
		img = Gtk.Image.new_from_icon_name(ICOOK, iNSz)
		btnok.set_image(img)
		self.grid.attach(btnok, 0, 1, 1, 1)
		btnccl = Gtk.Button(label="Cancel", halign=Gtk.Align.FILL, expand=True)
		btnccl.set_always_show_image(True)
		img = Gtk.Image.new_from_icon_name(ICOCCL, iNSz)
		btnccl.set_image(img)
		self.grid.attach(btnccl, 1, 1, 1, 1)
		btnok.connect('clicked', self.accept)
		btnccl.connect('clicked', self.exit)
		self.add(self.grid)
		self.show_all()
		PrefWin = self

	def accept(self, btn, data=None):
		"""Accept changes in Preferences and close window."""
		## save the changes to config.ini
		self.destroy()
		PrefWin = None

	def exit(self, btn, data=None):
		"""Close Preferences window discarding changes."""
		self.destroy()
		PrefWin = None
##===============================================================
##	ABOUT DIALOG
##===============================================================
class About(Gtk.AboutDialog):
	"""About dialog."""
	def __init__(self, wnd):
		Gtk.AboutDialog.__init__(self)
		global AboutWin
		self.set_transient_for(wnd)
		self.set_name(APPNAME)
		self.set_title(APPNAME)
		self.set_program_name(APPNAME)
		self.set_version(APPVERSION)
		self.set_icon(mainIcon)
		self.set_logo(mainIcon)
		self.set_copyright('🄯 %s' %(RELDATE))
		self.set_license(self.readfile('/usr/share/doc/drivemapper/copyright'))
		self.set_website_label('GitLab page')
		self.set_website('https://gitlab.com/linux-generic-and-cinnamon/generic-apps/drivemapper')
		self.set_authors(['Drugwash <linuxmintuser@disroot.org>'])
		self.set_comments('Manage your mapped network shares')
		AboutWin = self
		self.run()
		self.destroy()
		AboutWin = None

	def readfile(self, fpath):
		"""Read text from file."""
		try:
			f = open(os.path.abspath(fpath), 'r')
			text = f.read()
			f.close()
		except Exception:
			text = 'Cannot open copyright file.\nFind GPL v2/WTFPL v2 on the web.'
		return text
##===============================================================
class Application(Gtk.Application):
	"""Start Main application."""
	def __init__(self):
		Gtk.Application.__init__(self)

	def do_activate(self):
		"""Gtk.Application build main window."""
		global change
		self.win = MainWindow(self)
		self.win.show_all()
		wm, wn = self.win.get_preferred_size()
		if debug:
			print("keep size = %s (%dx%d at %dx%d)" % (keepSize, mainWidth, mainHeight, mainX, mainY))
		hints = Gdk.Geometry()
		if noResize:
			hints.min_width = wm.width
			hints.max_width = wm.width
			hints.min_height = wm.height
			hints.max_height = wm.height
			mask = Gdk.WindowHints.MIN_SIZE | Gdk.WindowHints.MAX_SIZE
		else:
			hints.min_width = 350
			hints.min_height = 150
			mask = Gdk.WindowHints.MIN_SIZE
		self.win.set_geometry_hints(None, hints, mask)
		if keepSize and mainWidth != 0 and mainHeight != 0:
			self.win.resize(mainWidth, mainHeight)
		if keepPos and mainX != -1 and mainY != -1:
			self.win.move(mainX, mainY)
		change = False

	def do_startup(self):
		"""Gtk.Application start."""
		Gtk.Application.do_startup(self)

if not os.path.isfile(CONFIG):
	readDefaults()
else:
	readPreferences()
updateIcons()
initNotifier()
mntitem = MountItem()
if not isMinVer(platform.python_version(), "3.11"):
	GObject.threads_init()  # No longer needed since Python 3.11
app = Application()
exit_sts = app.run(sys.argv)
sys.exit(exit_sts)
