#!/usr/bin/env python3
from distutils.core import setup

setup(
    # Application name:
    name="Drive Mapper",

    # Version number (initial):
    version="1.0.19",

    # Application author details:
    author="Drugwash",
    author_email="linuxmintuser@disroot.org",

    # Packages
    #packages=["."],

    # Include additional files into the package
    #include_package_data=True,

    # Details
    url="https://gitlab.com/linux-generic-and-cinnamon/generic-apps/drivemapper",

    #
    # license="LICENSE.txt",
    description="Manage mapped network drives.",

    # long_description=open("README.txt").read(),

    # Dependent packages (distributions)
    #install_requires=[
    #    "python3-nmap",
    #],
)
