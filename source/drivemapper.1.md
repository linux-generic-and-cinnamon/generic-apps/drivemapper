% DRIVEMAPPER(1) drivemapper 1.0.19 alpha
% Drugwash
% December 7, 2023

# NAME
drivemapper - Manage mapped network drives

# SYNOPSIS
**drivemapper** [*no options*]

# DESCRIPTION
**Drive mapper** Helps managing mapped network drives.\
They can be mounted or unmounted easily through single clicks.

# OPTIONS
**drivemapper**
: No command line options.

# EXAMPLES
**drivemapper**
: Click any name in the list to map/unmap.

# EXIT VALUES
None

# BUGS
- alpha version, bugs may exist.

# COPYRIGHT
Copyleft Drugwash, 2023 December. Double license GPLv2+/WTFPLv2.\
This is free software. You are free to change and redistribute it.\
There is NO WARRANTY, to the extent permitted by law.
