Format: 3.0 (native)
Source: drivemapper
Binary: drivemapper
Architecture: all
Version: 1.0.14
Maintainer: Drugwash <linuxmintuser@disroot.org>
Homepage: https://github.com/Drugwash2
Standards-Version: 4.6.0.1
Build-Depends: debhelper (>= 10), python3-all (>= 3.6), dh-python (>= 0.8)
Package-List:
 drivemapper deb python optional arch=all
Checksums-Sha1:
 86bcfb7c39cb5a34bd3eda5e2c744e7e8ae98622 67908 drivemapper_1.0.14.tar.xz
Checksums-Sha256:
 1b7b7fc169f908eaf28f80c55f4957ec614ca76ae914d1942b86b5ab9d0d2368 67908 drivemapper_1.0.14.tar.xz
Files:
 53ccc99ab141c20f79a37c6a345cfc1b 67908 drivemapper_1.0.14.tar.xz
Python3-Version: >= 3.6
