Format: 3.0 (native)
Source: drivemapper
Binary: drivemapper
Architecture: all
Version: 1.0.19
Maintainer: Drugwash <linuxmintuser@disroot.org>
Homepage: https://gitlab.com/Drugwash
Standards-Version: 4.6.0.1
Build-Depends: debhelper (>= 10), python3-all (>= 3.6), dh-python (>= 0.8)
Package-List:
 drivemapper deb python optional arch=all
Checksums-Sha1:
 c3f8ef65a3783903e5e51019f7e3a4f39f7dcdde 30108 drivemapper_1.0.19.tar.xz
Checksums-Sha256:
 fcdff1d454659653330f78fd154e342cee1cd086899e963632c039faffbd9bfa 30108 drivemapper_1.0.19.tar.xz
Files:
 a5d2da61ad685f619b1568ae6af12804 30108 drivemapper_1.0.19.tar.xz
Python3-Version: >= 3.6
